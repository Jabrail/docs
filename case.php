<?php
header('Content-Type: application/json');
$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/class.cases.php");
require_once($rootPath."/includes/class.user.php");
require_once($rootPath."/includes/class.package.php");
require_once($rootPath."/includes/newspaper.php");
require_once($rootPath."/includes/class.work.php");


$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

$token = isset($_GET["token"]) && !empty($_GET["token"]) ? $_GET["token"] : false;

$user = new User();
if (!$user->init($token)) return false;

if ($action == 'add_cases') {
    if ($_POST['npers'] == '111'){

        $case = new Cases();
        $case->newCase();
        $case->name = 'Иван';
        $case->last_name = 'Иванов';
        $case->patronymic = 'Иванович';
        $case->npers = '111-111-111-11';
        $case->creator_user_id = $user->user_id;

    } else {
        $conn = 'DRIVER={IBM DB2 ODBC DRIVER};DATABASE=ROS;HOSTNAME=10.17.0.98;PORT=50000;PROTOCOL=TCPIP;UID=db2docs;PWD=ptkDocs67';
        $conn = db2_connect($conn, '', '');
        if (!$conn) {
            echo 'Не удалось подключиться к БД ' . $db;
        }

        $FIO = getFIO($conn, $_POST['npers']);
        $case = new Cases();
        $case->newCase();
        $case->name = iconv('windows-1251', 'UTF-8',  $FIO['name']);
        $case->last_name = iconv('windows-1251',  'UTF-8', $FIO['last_name']);
        $case->patronymic = iconv('windows-1251', 'UTF-8',  $FIO['patronymic']);
        $case->npers = $_POST['npers'];
        $case->creator_user_id = $user->user_id;
    }
    $case->type = $_POST['type_id'];
    $case->update();
    $work = new Work();
    $work->newWorkRel($case->case_id, 1);
    $response = array($case->case_json());
    echo json_encode($response);


} elseif ($action == 'show') {

    if (isset($_GET['id'])) {

        $case = new Cases();
        $case->init($_GET['id']);
        $response = array($case->case_json());
        echo json_encode($response);
    }
} elseif ($action == 'get_all') {

    $stage_id = isset($_GET['stage_id']) ? $_GET['stage_id'] : 0;

    if ($user->checkStage($stage_id)) {
        $case = new Cases();

        $sort = '';
        if (isset($_GET['sort'])) {

            $sort = $_GET['sort'] == 'sort_by_id' ? ' ORDER BY cases.case_id DESC' : '';
        }

        if ($_GET['first'] == '1') {
            $where = " ";
        } else {
            $ids = $_GET['ids'];
            $where = " AND  case_id NOT IN($ids)> '$time'";
        }

        if ($_GET['status'] == '1') {
            $status = 1;
        } else {
            $status = 0;
        }


        $where = $where.'AND stage_work.stage_id = ' .$stage_id;

        $response = $case->case_get_all_json($sort, $where, $status);

        echo json_encode($response);

    }
    else {
        $error = Array();
        $error['error'] = 'access denied';
        $error['error'] = 'access denied';
        $error['key'] = 101;
        echo json_encode(Array($error));
    }

}

function getFIO($conn, $npers){

    $queryStr = "SELECT FA, IM, OT FROM PF.MAN WHERE PF.MAN.NPERS = '$npers'";
    $resQuery = FALSE;
    $_stmt = db2_prepare($conn, $queryStr);
    if ($_stmt) {
        if (db2_execute($_stmt)) {
            while ($row = db2_fetch_assoc($_stmt)) {
                $resQuery['last_name'] = $row['FA'];
                $resQuery['name'] = $row['IM'];
                $resQuery['patronymic'] = $row['OT'];
            }
        }
        db2_free_stmt($_stmt);
    }
    return $resQuery;
}
?>
