<?php

class Stage {

    public $stage_id = 0;
    public $stage_name = '';

    public function __construct()
    {

    }

    public function init($id = 0) {

        global $db;

        $sql = 'SELECT * FROM stage WHERE stage_id = '.$id;

        $result = $db->sql_query($sql);


        if ($row = $db->sql_fetchrow($result)) {

            $this->stage_id = $row['stage_id'];
            $this->stage_name = $row['stage_name'];
        }


    }

    public function stage_json() {

        $result = array();

        $result['stage_id'] = $this->stage_id;
        $result['stage_name'] = $this->stage_name;

        return $result;
    }

    public function getAll() {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM stage ";

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function allForUser($id) {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM user_rel LEFT JOIN stage ON user_rel.stage_id = stage.stage_id LEFT JOIN type_rel ON user_rel.stage_id = type_rel.stage_id WHERE user_rel.user_id = ".$id;
        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;

    }

    public function addForUser($user_id, $stage_id) {

        global $db;

        $sql = "INSERT INTO `user_rel`(`user_id`,`stage_id`) VALUES ('$user_id','$stage_id')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }


        return true;
    }
    public function removeForUser($id) {

        global $db;

        $sql = "DELETE FROM user_rel WHERE user_rel_id = $id ";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            return true;
        }

    }

    public function update() {

        global $db;

        $sql = "UPDATE stage
						SET stage_name = '{$this->stage_name}'
						WHERE stage_id = '{$this->stage_id}'";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }

    }

    public function newStage() {

        global $db;

        $date = time();
        $sql = "INSERT INTO stage (stage_name) VALUES ('')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            $this->init($db->sql_nextid());
        }
    }

    public function remove() {

        global $db;

        $sql = "DELETE FROM stage WHERE stage_id = ".$this->stage_id;

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            return true;
        }
    }
}

?>