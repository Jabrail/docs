<?php

include_once("class.base.php");
define("TAXI_IMAGE_PATH", $rootPath.FILES_DIR."taxi/");

class Taxi extends Base
{
	
	public $ItemImagePrefix    = "taxi-";
	public $ItemImagePath      = TAXI_IMAGE_PATH;
	public $ItemImageUrl       = "";
	public $ItemImageMaxWidth  = 120;
	public $ItemImageMaxHeight = 120;
	
	public $DbFields = array();
	public $DbKey    = "taxi_id";
	public $DbTable  = TABLE_TAXI;
	public $DbAlias  = "ta";
	
	public function SetItemDbFields()
	{
		$dbField['name'] = 'Call';
		$dbField['field'] = 'taxi_called';
        $dbField['type'] = 'flag';
        $dbField['edit'] = true;
        $dbField['empty'] = true;
        $dbField['default'] = 0;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Комментарий';
        $dbField['field'] = 'comment';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['edit'] = true;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Ф.И.';
		$dbField['field'] = 'taxi_name';
		$dbField['type'] = 'text';
		$dbField['empty'] = false;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Фамилия';
		$dbField['field'] = 'taxi_last_name';
		$dbField['type'] = 'text';
		$dbField['empty'] = false;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Логин пароль';
		$dbField['field'] = 'taxi_login';
		$dbField['type'] = 'text';
		$dbField['empty'] = false;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Пароль';
		$dbField['field'] = 'taxi_password';
		$dbField['type'] = 'text';
		$dbField['empty'] = false;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Телефон';
		$dbField['field'] = 'taxi_phone';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Марка';
        $dbField['field'] = 'taxi_auto';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['hint'] = 'ВАЗ';
        $dbField['edit'] = true;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Год';
        $dbField['field'] = 'taxi_year';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['hint'] = '1999.10.10';
        $dbField['edit'] = true;
        $dbField['show'] = false;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Компания';
		$dbField['field'] = 'admin_id';
		$dbField['type'] = 'key';
		$dbField['key_table'] = TABLE_ADMIN;
		$dbField['key_alias'] = 'a';
		$dbField['key_value'] = 'admin_company';
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Страна';
		$dbField['field'] = 'taxi_country';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = 'Россия';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Город';
		$dbField['field'] = 'taxi_city';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = 'Махачкала';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;


        $dbField['name'] = 'Текущая страна';
        $dbField['field'] = 'taxi_current_country';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['hint'] = 'Россия';
        $dbField['edit'] = true;
        $dbField['show'] = false;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Город';
		$dbField['field'] = 'taxi_current_city';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = 'Махачкала';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;



		$dbField['name'] = 'Модель';
		$dbField['field'] = 'car_brand';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = '21099';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Номера';
		$dbField['field'] = 'taxi_numbers';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['hint'] = 'А 645 ВЛ';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Цвет';
		$dbField['field'] = 'taxi_color';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Класс Авто';
		$dbField['field'] = 'taxi_class';
		$dbField['kost'] = 'taxi_class';
		$dbField['type'] = 'text';
		$dbField['edit'] = false;
		$dbField['empty'] = true;
		$dbField['default'] = '1';
		$dbField['value'] = '1';
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Стандарт';
        $dbField['field'] = 'taxi_class_standard';
        $dbField['type'] = 'flag';
        $dbField['edit'] = true;
        $dbField['empty'] = true;
        $dbField['default'] = 1;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Класс комфорт';
        $dbField['field'] = 'taxi_class_comfort';
        $dbField['type'] = 'flag';
        $dbField['edit'] = true;
        $dbField['empty'] = true;
        $dbField['default'] = 0;
        $dbField['show'] = false;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

        $dbField['name'] = 'Класс бизнес';
        $dbField['field'] = 'taxi_class_business';
        $dbField['type'] = 'flag';
        $dbField['edit'] = true;
        $dbField['empty'] = true;
        $dbField['default'] = 0;
        $dbField['show'] = false;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Не курит';
		$dbField['field'] = 'taxi_nosmoking';
		$dbField['type'] = 'flag';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Детское кресло';
		$dbField['field'] = 'taxi_child';
		$dbField['type'] = 'flag';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Кондиционер';
		$dbField['field'] = 'taxi_condition';
		$dbField['type'] = 'flag';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'На работе';
		$dbField['field'] = 'taxi_on';
		$dbField['type'] = 'flag';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = false;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Баланс';
		$dbField['field'] = 'taxi_balance';
		$dbField['type'] = 'text';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = "Занят";
		$dbField['field'] = 'taxi_busy';
		$dbField['type'] = 'flag';
		$dbField['edit'] = false;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = false;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = "Бан";
		$dbField['field'] = 'taxi_banned';
		$dbField['type'] = 'flag';
		$dbField['edit'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['show'] = true;
		$dbField['ajax'] = true;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Дата создания';
		$dbField['field'] = 'taxi_registered';
		$dbField['type'] = 'date';
		$dbField['time'] = true;
		$dbField['empty'] = true;
		$dbField['default'] = 0;
		$dbField['hint'] = 'Дата создания аккаунта такси';
		$dbField['edit'] = true;
		$dbField['disabled'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;
		
		$dbField['name'] = 'Версия приложения';
		$dbField['field'] = 'taxi_version';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['disabled'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Взятые заказы';
		$dbField['field'] = 'complete_orders';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['edit'] = false;
		$dbField['disabled'] = true;
		$dbField['show'] = true;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Token';
		$dbField['field'] = 'taxi_token';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['disabled'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;

		$dbField['name'] = 'Registration Id';
		$dbField['field'] = 'registration_id';
		$dbField['type'] = 'text';
		$dbField['empty'] = true;
		$dbField['default'] = '';
		$dbField['edit'] = true;
		$dbField['disabled'] = true;
		$dbField['show'] = false;
		$dbField['ajax'] = false;
		$this->DbFields[] = $dbField; $dbField = false;


        $dbField['name'] = 'crew_id';
        $dbField['field'] = 'taxi_crew_id';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['edit'] = true;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;


        $dbField['name'] = 'Позывной';
        $dbField['field'] = 'taxi_callsign';
        $dbField['type'] = 'text';
        $dbField['empty'] = true;
        $dbField['default'] = '';
        $dbField['edit'] = true;
        $dbField['show'] = true;
        $dbField['ajax'] = true;
        $this->DbFields[] = $dbField; $dbField = false;
		
		return true;
	}
	
	public function SetAdminConfig()
	{
		$this->ItemConfig['itemName']    = "Такси"; // Item name
		$this->ItemConfig['itemNames']   = "Такси"; // Item plural name
		$this->ItemConfig["adminScript"] = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], "/")+1); // Running file
		$this->ItemConfig["classFile"]   = "class.case.php"; // Current class file
		$this->ItemConfig["className"]   = get_class($this); // Current class name
		$this->ItemConfig["showImage"]   = true; // Show image
		$this->ItemConfig["imagePath"]   = $this->ItemImageUrl; // Image path
		$this->ItemConfig["adminTpl"]    = "taxi.tpl"; // Template file
	}
	
	public function SaveItemImage($id, $imagePath="")
	{
		$imagePath = $imagePath ? $imagePath : $this->ItemImagePath;
        $imagePathDocs = $imagePath.'/docs/';
        $imagePathCars = $imagePath.'/cars/';

		if(isset($_FILES["image"]) && $_FILES["image"]["name"])
		{
			if($fileName = $this->UploadItemImage($_FILES["image"], $this->ItemImagePrefix.$id, $imagePath.""))
			{
				$this->ResizeImage($imagePath."original".$fileName, $imagePath.$fileName, $this->ItemImageMaxWidth, $this->ItemImageMaxHeight, true, true, true);
				return $fileName;
			}
			else
			{
				return false;
			}
		}

        if(isset($_FILES["photo_docs"]) && $_FILES["photo_docs"]["name"])
        {
            if($fileName = $this->UploadItemImage($_FILES["photo_docs"], $this->ItemImagePrefix.$id, $imagePathDocs.""))
            {
                return $fileName;
            }
            else
            {
                return false;
            }
        }
        if(isset($_FILES["photo_car"]) && $_FILES["photo_car"]["name"])
        {
            if($fileName = $this->UploadItemImage($_FILES["photo_car"], $this->ItemImagePrefix.$id, $imagePathCars.""))
            {
                return $fileName;
            }
            else
            {
                return false;
            }
        }

        return "";
	}


	
	public function DeleteItemImage($fileName, $imagePath="")
	{
		$imagePath = $imagePath ? $imagePath : $this->ItemImagePath;
		
		if(@unlink($imagePath.$fileName) && @unlink($imagePath."original/".$fileName))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function ValidateItem()
	{
		global $db, $rootPath;
		
		$login = trim($_POST["taxi_login"]);
		
		$id = $_POST[$this->DbKey] ? $_POST[$this->DbKey] : 0;
		$sql = "SELECT taxi_id
						FROM {$this->DbTable}
						WHERE taxi_login = '{$login}' AND {$this->DbKey} <> {$id}";
		if(!$result = $db->sql_query($sql))
		{
			$this->SetError("Ошибка при выборе записей", __FILE__, __LINE__, $db->sql_error());
			return false;
		}
		elseif($db->sql_numrows($result))
		{
			$this->SetError("Логин - ".$login." уже используется.", __FILE__, __LINE__);
		}
			
		return parent::ValidateItem();
	}
	
	public function GetJoinItems($fieldKey)
	{
		global $db;
		
		$where = $fieldKey == "admin_id" ? " WHERE admin_group = 3 " : "";
		
		if(!is_numeric($fieldKey))
		{
			foreach($this->DbFields as $key=>$value)
			{
				if($value['field'] == $fieldKey)
				{
					$fieldKey = $key;
					break;
				}
			}
		}
		
		$sql = "SELECT {$this->DbFields[$fieldKey]['field']}, {$this->DbFields[$fieldKey]['key_value']}
						FROM {$this->DbFields[$fieldKey]['key_table']}".$where;
		if(!$result = $db->sql_query($sql))
		{
			$this->SetError("Ошибка при выборе записи", __FILE__, __LINE__, $db->sql_error());
			return false;
		}
		elseif($db->sql_numrows($result))
		{
			$joinItems = $db->sql_fetchrowset($result);
			$db->sql_freeresult($result);
			return $joinItems;
		}
		
		return "";
	}
	
	public function PrepareInsertFields()
	{
		return parent::PrepareInsertFields().",taxi_registered".($_SESSION["admin"]["admin_group"] == 2 ? ",admin_id" : "");
	}
	
	public function PrepareInsertValues($fieldsValues)
	{
		return parent::PrepareInsertValues($fieldsValues).",".time().($_SESSION["admin"]["admin_group"] == 2 ? ",".$_SESSION["admin"]["admin_id"] : "");
	}
	
}

?>