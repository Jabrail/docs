<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 9:13
 */

class Work {

    public $WR_id = 0;
    public $type_id = 0;
    public $case_id = '';
    public $stage_id = '';
    public $user_id = '';
    public $work_status = '';
    public $time = '';

    public function __construct()
    {

    }


    public function takeInWork($case_id, $user_id) {

        $work = $this->getStage($case_id);
        $stage_id = $work['stage_id'];
        $stage_num = $work['stage_num'];
        $type_id = $this->getType($case_id);
         $this->add($case_id, $stage_num, $stage_id, $user_id, 1, $type_id);


    }

    public function sendNextStage($case_id, $user_id) {

        $work = $this->getStage($case_id);
        $stage_id = $work['stage_id'];
        $stage_num = $work['stage_num'];
        $type_id = $work['type_id'];
        $stages = $this->getTypeStages($type_id);

        $this->add($case_id, $stage_num, $stage_id, $user_id, 2, $type_id);

        if (($stage_num) != count($stages)) {

            $stage_id = $stages[$stage_num + 1]['stage_id'];
            $this->add($case_id, ($stage_num + 1), $stage_id, $user_id, 0, $type_id);

        }



    }

    public function newWorkRel($case_id, $user_id) {

        $type_id = $this->getType($case_id);
        $workStages = $this->getTypeStages($type_id);

        $stage_num = 0;
        $stage_id =  $workStages[0]['stage_id'];

        $this->add($case_id, $stage_num, $stage_id, $user_id, 0, $type_id);
    }

    public function remove() {

        global $db;

        $sql = "DELETE FROM types WHERE type_id = ".$this->type_id;

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            return true;
        }
    }

    private function add($case_id,$stage_num,$stage_id,$user_id,$work_status, $type_id) {

        global $db;

        $date = time();
        $sql = "INSERT INTO `stage_work`(`case_id`, `stage_num`, `stage_id`, `user_id`, `work_status`, `type_id`, `time` ) VALUES ('$case_id','$stage_num','$stage_id','$user_id','$work_status','$type_id', '$date')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {

            return true;

        }

    }

    private function getTypeStages($type_id) {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM type_rel WHERE type_id = ".$type_id;

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;

    }
    private function getType($case_id) {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM cases WHERE case_id = ".$case_id;

         if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

         return $result[0]['type_id'];

    }
    private function getStage($case_id) {

        global $db;
        $result = 0;

        $sql = "SELECT *
						FROM stage_work WHERE case_id = ".$case_id." ORDER BY id DESC LIMIT 1";

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }
        else {
            var_dump($db->sql_error($sql));
        }

        return $result[0];

    }
}

?>