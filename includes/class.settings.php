<?php

class Settings
{
	
	public $InAdmin  = false;
	public $Valid    = true;
	public $Error    = "";
	public $Info     = "";
	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		if(strpos($_SERVER['PHP_SELF'], "admin/") !== false) $this->InAdmin = true;
	}
	
	public function SettingsSubmitted()
	{
		return (isset($_POST["submit_settings"]))? true: false;
	}
	
	public function SaveSettings()
	{
		global $db;
		
		$updateSql = "";
		
		foreach($_POST as $key=>$value)
		{
			if($key != "submit_settings")
			{
				$updateSql .= "$key = '$value',";
			}
		}
		$updateSql = substr($updateSql, 0, strlen($updateSql)-1);
		
		$sql = "UPDATE ".TABLE_SETTINGS."
						SET $updateSql
						WHERE settings_id = 1";
		if(!$result = $db->sql_query($sql))
		{
			$this->SetError("Ошибка при обновлении настроек", __FILE__, __LINE__, $db->sql_error());
			return false;
		}
		else
		{
			$this->SetInfo("Настройки сохранены", __FILE__, __LINE__);
			return true;
		}
		
		return false;
	}
	
	public function GetSettings($settingsId = 1)
	{
		global $db;
		
		$sql = "SELECT *
						FROM ".TABLE_SETTINGS."
						WHERE settings_id = $settingsId";
		if(!$result = $db->sql_query($sql))
		{
			$this->SetError("Ошибка при выборе настроек", __FILE__, __LINE__, $db->sql_error());
			return false;
		}
		else
		{
			if($db->sql_numrows($result))
			{
				$settings = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
				return $settings;
			}
		}
		
		return false;
	}
	
	public function SetError($error, $file = false, $line = false, $sql = false, $valid = false, $log = 1)
	{
		$errorText = "<div>";
		if($error) $errorText .= $error;
		if($file)  $errorText .= " | File: ".$file;
		if($line)  $errorText .= " | Line: ".$line;
		if($sql)   $errorText .= " | SQL Error #".$sql["code"].": ".$sql["message"];
		$errorText .= "</div>\n";
		
		if($log) $this->WriteLog(2, $error, $file, $line, $sql);
		
		$this->Valid  = $valid;
		if($log == 1) $this->Error .= $errorText;
	}
	
	public function SetInfo($info, $file = false, $line = false, $log = 1)
	{
		$infoText = "<div>";
		if($info != "") $infoText .= $info;
		$infoText .= "</div>\n";
		
		if($log) $this->WriteLog(1, $info, $file, $line);
		
		if($log == 1) $this->Info .= $infoText;
	}
	
	public function WriteLog($status, $text, $file = false, $line = false, $sql = false)
	{
		global $db;
		
		$admin      = $this->InAdmin && isset($_SESSION["admin"]) ? $_SESSION["admin"]["admin_name"] : "";
		$phpSelf    = $_SERVER['PHP_SELF'];
		$text       = $db->sql_escape($text);
		$file       = $file ? $db->sql_escape($file) : "";
		$line       = $line ? $line : 0;
		$sqlCode    = $sql ? $db->sql_escape($sql["code"]) : "";
		$sqlMessage = $sql ? $db->sql_escape($sql["message"]) : "";
		$date       = time();
		
		$sql = "INSERT INTO ".TABLE_LOG."
							(log_admin, log_status, log_php_self, log_text, log_file, log_line, log_sql_code, log_sql_message, log_date)
						VALUES
							('$admin', $status, '$phpSelf', '$text', '$file', $line, '$sqlCode', '$sqlMessage', $date)";
		if(!$result = $db->sql_query($sql))
		{
			$this->SetError("Ошибка при логгировании", __FILE__, __LINE__, $db->sql_error(), false, false);
			return false;
		}
		else
		{
			return true;
		}
		
		return "";
	}
	
}

$settingsObj = new Settings();
$settings = $settingsObj->GetSettings();

?>