<?php

class Cases {

    public $case_id = 0;
    public $name = '';
    public $last_name = '';
    public $patronymic = '';
    public $type = '';
    public $date_create = '';
    public $npers = '';

    public function __construct()
    {

    }

    public function init($id = 0) {

        global $db;

        $sql = 'SELECT * FROM cases WHERE case_id = '.$id;

        $result = $db->sql_query($sql);


        if ($row = $db->sql_fetchrow($result)) {

            $this->case_id = $row['case_id'];
            $this->name = $row['case_name'];
            $this->last_name = $row['case_last_name'];
            $this->patronymic = $row['case_patronymic'];
            $this->type = $row['type_id'];
            $this->date_create = $row['case_create_date'];
            $this->npers = $row['npers'];

           }


    }

    public function case_json() {

        $result = array();

        $result['case_id'] = $this->case_id;
        $result['case_name'] = $this->name;
        $result['case_last_name'] = $this->last_name;
        $result['case_patronymic'] = $this->patronymic;
        $result['type_id'] = $this->type;
        $result['case_create_date'] = $this->date_create;
        $result['npers'] = $this->npers;

        return $result;
    }

    public function case_get_all_json($sort, $where, $status) {

        global $db;
        $result = array();

        $sql = "SELECT * FROM cases LEFT JOIN stage_work ON cases.case_id = stage_work.case_id
                       WHERE cases.case_id = stage_work.case_id $where
                              GROUP BY stage_work.case_id
                                      HAVING MAX(stage_work.work_status) = $status
                                      $sort";
/*
        $sql_exception_count = "SELECT *
						FROM cases WHERE case_id > ".$max;*/

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function update() {

        global $db;

        $sql = "UPDATE cases
						SET case_name = '{$this->name}',
						case_last_name = '{$this->last_name}',
						case_patronymic = '{$this->patronymic}',
						case_create_date = '{$this->date_create}',
						npers = '{$this->npers}',
						type_id = '{$this->type}'
						WHERE case_id = '{$this->case_id}'";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }

    }

    public function newCase() {

        global $db;

        $date = time();
        $sql = "INSERT INTO cases (case_create_date) VALUES (".$date.")";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            $this->init($db->sql_nextid());
        }
    }

    public function deleteCase() {

        global $db;

        $sql = "DELETE FROM cases WHERE case_id = ".$this->case_id;

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            return true;
        }
    }
}

?>