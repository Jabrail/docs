<?php

class TaxiResponder
{
    public $DbTable = TABLE_TAXI;
    public $Valid = true;

    public $StatusCode = 200;
    public $StatusMessage = "OK";

    public $ResponseData = false;

    public $ErrorCode = 0;
    public $ErrorMessage = "";

    public $Taxi = false;

    public $RadiusTaxiSearch = 0;
    public $TaxiActivityTimeout = 0;
    public $OrderActivityTimeout = 0;
    public $ReservationDelay = 0;
    public $Timeout_taxi_order = 0;
    public $TaxiLiderBusy = 0;

    public function __construct($action = false)
    {
        global $settings;

        $this->RadiusTaxiSearch = $settings["radius"];
        $this->TaxiActivityTimeout = $settings["timeout_taxi"];
        $this->OrderActivityTimeout = $settings["timeout_order"] ? $settings["timeout_order"] * 60 : 20 * 60;
        $this->ReservationDelay     = $settings["reservation_delay"] ? $settings["reservation_delay"] * 60 : 20 * 60;
        $this->Timeout_taxi_order     = $settings["timeout_taxi_order"];
        $this->TaxiLiderBusy = 0;


        $this->CheckApiKey();
        if($this->Valid && $action != "auth" && $action != "post_reg") $this->CheckToken($action);

        return $this->Valid;
    }

    public function CheckBusyLider() {

        if ($this->Taxi['taxi_crew_id'] != 0 ) {

            $crew_id = $this->Taxi['taxi_crew_id'];
            $signature = md5('crew_id='.$crew_id.'M37G6H1t3pdsq1XY0yI7Yqx8s5kYzW948zHJ9z6O');

            if( $curl = curl_init() ) {

                curl_setopt($curl, CURLOPT_URL, 'https://lider05taxi.ru:8089/common_api/1.0/get_crews_coords?crew_id='.$crew_id);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Signature: '.$signature));
                $out = curl_exec($curl);

                if (strpos($out, "state_kind")) {
                    $i = strpos($out, "waiting");
                    if ($i === false ) {
                        $this->TaxiLiderBusy = 1;
                    } else {
                        $this->TaxiLiderBusy = 0;
                    }
                } else {
                    $this->TaxiLiderBusy = 0;
                }
            }
        }
    }

    public function CheckToken($action)
    {
        $token = isset($_POST["token"]) ? $_POST["token"] : ( isset($_GET["token"]) ? $_GET["token"] : false );

        if(!$token)
        {
            $this->SetError(3, "Token не указан");
            return false;
        }
        elseif(!$this->IsTokenExist($token))
        {
            $this->SetError(4, "Token не найден ".$action.'  '.$token);
            return false;
        }

        return true;
    }

    public function CheckApiKey()
    {
        $apiKey = isset($_POST["api_key"]) ? $_POST["api_key"] : ( isset($_GET["api_key"]) ? $_GET["api_key"] : false );

        if(!$apiKey)
        {
            $this->SetError(2, "API приложения не указан");
            return false;
        }

        return true;
    }

    public function TaxiLogin()
    {
        global $db;

        if(!isset($_POST["user_name"]))
        {
            $this->SetError(10, "Имя пользователя не указано");
            return false;
        }

        if(!isset($_POST["user_password"]))
        {
            $this->SetError(11, "Пароль пользователя не указан");
            return false;
        }

        $user_name = trim(str_replace('+', '', $_POST['user_name']));
        /*if (substr($user_name,0,1) == "8") {
            $user_name = substr($user_name, 1);
        }*/
        $sql = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_login = '".$user_name."' AND taxi_password = '".$_POST['user_password']."'";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        else
        {
            if($db->sql_numrows($result))
            {
                $taxi = $db->sql_fetchrow($result);
                $db->sql_freeresult($result);
                $token = $this->UpdateTaxiToken($user_name);
                if($token) $this->ResponseData["token"] = $token;
                return true;
            }
            else
            {
                $this->SetError(12, "Такси не найден");
                return false;
            }
        }

        return true;
    }

    public function TaxiReg()
    {
        global $db, $settings;
        include_once("includes/class.base.php");

        /*        if(!isset($_POST["phone"])) { $this->SetError(-1, "Параметр phone не указан"); return false; }
                if(!isset($_POST["photo_driver"])) { $this->SetError(-1, "Фото не указано"); return false; }
                if(!isset($_POST["photo_docs"])) { $this->SetError(-1, "Фото документов не указано"); return false; }
                if(!isset($_POST["photo_car"])) { $this->SetError(-1, "Фото автомобиля не указано"); return false; }
                if(!isset($_POST["car_model"])) { $this->SetError(-1, "Модель не указана"); return false; }
                if(!isset($_POST["car_color"])) { $this->SetError(-1, "Цвет не указан"); return false; }
                if(!isset($_POST["car_number"])) { $this->SetError(-1, "Номер автомобиля не указан"); return false; }*/

        $phone = $_POST["phone"];

        $name = $_POST["name"];
        $last_name = $_POST["last_name"];
        $car_model = $_POST["car_model"];
        $car_brand = $_POST["car_brand"];
        $car_color = $_POST["car_color"];
        $car_number = $_POST["car_number"];
        $car_smoke = $_POST["car_smoke"];
        $car_child = $_POST["car_child"];
        $car_condi = $_POST["car_condi"];
        $taxi_year = $_POST["year"];
        $city = mb_convert_case($_POST["city"], MB_CASE_TITLE, "UTF-8");
        $start_sum = $settings['taxi_start_sum'];
        if (isset($_POST["country"])) {
            $country = $_POST["country"];
        }
        else {
            $country = '';
        }
        if (isset($_POST["company"])) {
            $company = $_POST["company"];
            $company = strtolower($company);
        }
        else {
            $company = 0;
        }

        if (isset($_POST["year"])) {
            $taxi_year = $_POST["year"];
        }
        else {
            $taxi_year = '0';
        }

        $company = strtolower($company);
        $company = trim($company);

        $sql_admin = 'SELECT *
                        FROM admin
                        WHERE  promo_cod LIKE "%'.$company.'%"';

        if(!$result = $db->sql_query($sql_admin))
        {
            $admin_id = 0;
        }
        elseif($db->sql_numrows($result))
        {
            $admin = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            $admin_id = $admin['admin_id'];
        } else {
            $admin_id = 0;
        }


        $sql = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_phone = '{$_POST['phone']}'";

        $result = $db -> sql_query($sql);
        if($db -> sql_numrows($result))
        {
            $this->SetError(101, "Номер уже зарегистрирован", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        else
        {
            $phone=str_replace("+","",$phone);
            $taxi_phone = '+'.$phone;
            $pass = $this -> generatePassword(4);
            $sql_create = "INSERT INTO $this->DbTable
							( taxi_password , taxi_login , taxi_name , taxi_last_name , taxi_phone, taxi_auto, car_brand, taxi_numbers, taxi_color, taxi_nosmoking, taxi_child, taxi_condition, taxi_class , taxi_class_standard , taxi_class_comfort , taxi_class_business, taxi_country , taxi_city  , admin_id , taxi_year , taxi_banned , taxi_first_login , taxi_balance)
						VALUES
							('$pass' , '$phone' , '$name' , ' $last_name', '$taxi_phone', '$car_model', '$car_brand', '$car_number', '$car_color' , '$car_smoke', '$car_child', '$car_condi' , '1', '1' , '0' , '0' , '$country' , '$city' , '$admin_id' , '$taxi_year' , '1' , '1' , '$start_sum')";
            if ($result_create = $db->sql_query($sql_create)) {

                $bs = new Base();
                $sql_new = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_phone = '{$_POST['phone']}'";

                $result_new = $db->sql_query($sql_new);
                $row = $db->sql_fetchrow($result_new);
                $bs->SaveItemDocsImage('taxi-'.$row['taxi_id'] , './files/taxi/');

                $taxi_id = $row['taxi_id'];
                $sql_rate_create = "INSERT INTO taxi_reviews
							(client_id, taxi_id, order_id, rate_number, rate_comment, rate_date)
						VALUES
							('1', '$taxi_id', '1', '4', 'good','1394611486')";
                $db->sql_query($sql_rate_create);

                require_once("./includes/class.sms.php");
                $sms = new SMS();
                $sms->SendSMS($taxi_phone, 'Логин водителя: '.$phone.'\n Пароль: '.$pass);

                return "";
            }
            else {
                return false;
            }
        }

        return "";
    }

    function generatePassword($length = 8){
        $chars = '23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    public function SetTaxiStatus()
    {
        global $db;

        if(!isset($_POST["online"])) { $this->SetError(41, "Отсутствует параметр online"); return false; }

        $status = $_POST["online"] === "true" || $_POST["online"] === true ? 1 : 0;

        if (isset($_POST["version"])) {
            $version = $_POST["version"];
        }
        else {
            $version = '0';
        }

        $sql = "UPDATE $this->DbTable
						SET taxi_on = {$status},
						taxi_version = {$version}
						WHERE taxi_id = '{$this->Taxi['taxi_id']}'";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        return true;
    }

    public function SaveGoogleRegistrationId()
    {
        global $db;

        if(!isset($_POST["google_id"])) { $this->SetError(31, "Отсутствует параметр google_id"); return false; }
        if(empty($_POST["google_id"])) { $this->SetError(32, "Параметр google_id пустой"); return false; }

        $sql = "UPDATE $this->DbTable
						SET registration_id = '{$_POST['google_id']}'
						WHERE taxi_id = '{$this->Taxi['taxi_id']}'";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        return true;
    }

    public function UpdateTaxiToken($login)
    {
        global $db;

        $token = md5($login.time());
        $sql = "UPDATE $this->DbTable
						SET taxi_token = '{$token}'
						WHERE taxi_login = '{$login}'";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении Token", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_affectedrows())
        {
            return $token;
        }

        return false;
    }

    public function IsTokenExist($token)
    {
        global $db;

        $sql = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_token = '{$token}'";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $this->Taxi = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            return true;
        }

        return false;
    }

    public function SetLocation()
    {
        global $db;

        if(!isset($_POST["lat"])) { $this->SetError(21, "Отсутствует параметр lat"); return false; }
        if(!isset($_POST["lon"])) { $this->SetError(22, "Отсутствует параметр lon"); return false; }

        if(empty($_POST["lat"])) { $this->SetError(23, "Параметр lat пустой"); return false; }
        if(empty($_POST["lon"])) { $this->SetError(24, "Параметр lon пустой"); return false; }

        $city = "";
        $regCity = "";
        $country = "";

        if(!empty($_POST["current_city"])) {
            $city = " taxi_current_city = '{$_POST["current_city"]}' ,";
        }
        if(!empty($_POST["current_country"])) {

            $country = " taxi_current_country = '{$_POST["current_country"]}' ,";
        }

        if ($this->Taxi['taxi_first_login'] == 1) {
            if ($this -> Taxi['taxi_city'] == '') {
                $regCity = " taxi_city = '{$_POST["current_city"]}' , taxi_first_login = 0 ,";
            }
            else {
                $regCity = " taxi_first_login = 0 ,";
            }

        }


        $sql = "UPDATE {$this->DbTable}
						SET lat = {$_POST["lat"]},
								lon = {$_POST["lon"]},
								taxi_on = 1 ,
								{$city}
								{$regCity}
								{$country}
								taxi_last_activity = ".time()."
						WHERE taxi_id = {$this->Taxi['taxi_id']}";

        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении гео данных", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        if($activeOrder = $this->GetActiveOrder()) { $this->CalculateOrder($activeOrder["order_id"], $_POST["lat"], $_POST["lon"]); }

        return true;
    }

    public function GetProfile()
    {
        global $db;

        $rating = null;

        $sql = "SELECT AVG(rate_number) AS rating
						FROM ".TABLE_TAXI_REVIEWS."
						WHERE taxi_id = {$this->Taxi["taxi_id"]}
						GROUP BY taxi_id";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $row = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            $rating = $row["rating"];
        }

        $profile["id"] = $this->Taxi["taxi_id"];
        $profile["first_name"] = $this->Taxi["taxi_name"];
        $profile["last_name"] = $this->Taxi["taxi_last_name"];
        $profile["rating"] = round($rating, 1);
        $profile["busy"] = $this->Taxi["taxi_busy"];
        $profile["balance"] = $this->Taxi["taxi_balance"];

        $this->ResponseData["profile"] = $profile;

        return true;
    }


    public function SetBusy()
    {
        global $db;

        if(!isset($_POST["status"])) { $this->SetError(21, "Отсутствует параметр status"); return false; }

        if(empty($_POST["status"])) { $this->SetError(23, "Параметр status пустой"); return false; }


        $busy = " taxi_busy = '{$_POST["status"]}' ,";




        $sql = "UPDATE {$this->DbTable}
						SET taxi_on = 1 ,
								{$busy}
								taxi_last_activity = ".time()."
						WHERE taxi_id = {$this->Taxi['taxi_id']}";

        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении статуса", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        //if($activeOrder = $this->GetActiveOrder()) { $this->CalculateOrder($activeOrder["order_id"], $_POST["lat"], $_POST["lon"]); }

        return true;
    }

    public function GetOrderStatus()
    {
        global $db, $rootPath;

        if(!isset($_POST["order_id"])) { $this->SetError(-1, "Отсутствует параметр order_id"); return false; }
        if(empty($_POST["order_id"])) { $this->SetError(-1, "Параметр order_id пустой"); return false; }

        $orderId = $_POST["order_id"];
        $order = false;

        $sql = "SELECT o.*, c.*, AVG(cr.rate_number) AS rating
						FROM ".TABLE_ORDERS." AS o
						LEFT JOIN ".TABLE_CLIENTS." AS c ON (o.client_id = c.client_id)
						LEFT JOIN ".TABLE_CLIENTS_REVIEWS." AS cr ON (o.client_id = cr.client_id)
						WHERE o.order_id = {$orderId}
						GROUP BY o.order_id";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $orderRow = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);

            $order["id"] = $orderRow["order_id"];
            $order["order_status"] = $orderRow["order_status"];
            $order["taxi_id"] = $orderRow["taxi_id"];
            $order["order_status_date"] = $orderRow["order_status_date"];
            $order["order_status_info"] = $orderRow["order_status_info"];
            //$order["lat"] = $orderRow["order_lat"];
            //$order["lon"] = $orderRow["order_lon"];
            //$order["first_name"] = $orderRow["client_name"];
            //$order["last_name"] = $orderRow["client_last_name"];
            //$order["phone"] = $orderRow["client_phone"];
            //$order["response_time"] = 10;
            //$order["street"] = $orderRow["order_street"];
            //$order["house"] = $orderRow["order_house"];
            //$order["building"] = $orderRow["order_building"];
            //$order["corpus"] = $orderRow["order_corpus"];
            //$order["entrance"] = $orderRow["order_entrance"];
            //$order["street"] = $orderRow["order_street"];
            //$order["destination"] = $orderRow["order_destination"];
            //$order["drive_time"] = 0;
            //$order["rating"] = round($orderRow["rating"], 1);
            //$order["time"] = $orderRow["order_time"];
            //$order["child"] = $orderRow["order_child"] ? true : false;
            //$order["nosmoking"] = $orderRow["order_nosmoking"] ? true : false;
            //$order["condition"] = $orderRow["order_condition"] ? true : false;
        }
        else
        {
            $this->SetError(-1, "Заказ #{$orderId} не найден", false);
            return false;
        }

        $this->ResponseData["order"] = $order;

        return true;
    }

    public function GetOrders()
    {
        global $db, $rootPath, $settings;

        $this->CheckBusyLider();


        $lat = $this->Taxi["lat"];
        $lon = $this->Taxi["lon"];

        $radius = $this->RadiusTaxiSearch;
        //      $timeout = time() - $this->Timeout_taxi_order;
        $timeout = time() - $this->OrderActivityTimeout;
        $reservationSql = " (o.order_time = 0 OR o.order_time < ".(time() + $this->ReservationDelay).") AND " ;

        require_once($rootPath.INC_DIR."class.tariffs.php");
        $tariffs = new Tariffs();

        if ($this->Taxi['taxi_current_city'] != '') {

            $sql = "SELECT * FROM tariffs WHERE tariff_name = '{$this->Taxi['taxi_current_city']}'";
        }
        else {
            $sql = "SELECT * FROM tariffs WHERE tariff_name = 'Махачкала'";
        }

        //    $id_tariff = 1;
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $taxiArr = $db->sql_fetchrowset($result);
            $taxiArr = array_reverse($taxiArr);
            $db->sql_freeresult($result);

            foreach($taxiArr as $key => $value)
            {
                $id_tariff = $value['tariff_id'];
            }
        }
        else {

            $sql = "SELECT * FROM tariffs WHERE tariff_name = '{$this->Taxi['taxi_city']}'";

            if(!$result = $db->sql_query($sql))
            {
                $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
                return false;
            }
            elseif($db->sql_numrows($result))
            {
                $taxiArr = $db->sql_fetchrowset($result);
                $taxiArr = array_reverse($taxiArr);
                $db->sql_freeresult($result);

                foreach($taxiArr as $key => $value)
                {
                    $id_tariff = $value['tariff_id'];
                }
            }

        }



        $orderTariff = $tariffs->GetItem($id_tariff);

        $taxi_info["banned"] = $this->Taxi['taxi_banned'];

        $tariff["start"] = $orderTariff["tariff_base_cost"];
        $tariff["free"] = $orderTariff["tariff_free_distance"];
        $tariff["city"] = $orderTariff["tariff_distance_cost"];
        $tariff["outofcity"] = $orderTariff["tariff_distance_outofcity_cost"];
        $tariff["minute"] = $orderTariff["tariff_delay_cost"];
        $tariff["minute_outofcity"] = $orderTariff["tariff_delay_outofcity_cost"];
        $tariff["premium_business"] = $orderTariff["premium_business"];
        $tariff["premium_condition"] = $orderTariff["premium_condition"];
        $tariff["premium_comfort"] = $orderTariff["premium_comfort"];


        $orders = array();

        $lat1 = $lat - ($radius / 111);
        $lat2 = $lat + ($radius / 111);
        $lon1 = $lon - $radius / abs(cos(deg2rad($lat)) * 111);
        $lon2 = $lon + $radius / abs(cos(deg2rad($lat)) * 111);

        $sql = "SELECT o.*, c.*, AVG(cr.rate_number) AS rating, 6371 * ACOS(COS(RADIANS(o.order_lat)) * COS(RADIANS({$lat})) * COS(RADIANS({$lon}) - RADIANS(o.order_lon)) + SIN(RADIANS(o.order_lat)) * SIN(RADIANS({$lat}))) AS distance
						FROM ".TABLE_ORDERS." AS o
						LEFT JOIN ".TABLE_CLIENTS." AS c ON (o.client_id = c.client_id)
						LEFT JOIN ".TABLE_CLIENTS_REVIEWS." AS cr ON (o.client_id = cr.client_id)
						WHERE o.order_status = 0 AND o.client_banned = 0 AND
									o.order_date > ".($timeout)." AND
									{$reservationSql}
									o.order_lat BETWEEN {$lat1} AND {$lat2} AND
									o.order_lon BETWEEN {$lon1} AND {$lon2}
						GROUP BY o.order_id
						HAVING distance < {$radius}
						ORDER BY o.order_date";

        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $taxiArr = $db->sql_fetchrowset($result);
            $taxiArr = array_reverse($taxiArr);
            $db->sql_freeresult($result);

            foreach($taxiArr as $key => $value)
            {

                if ($this->ExceptionTaxiStatus($value["order_id"], $this->Taxi["taxi_id"], $value['order_time'] , 0)) {

                    $filter = true;

                    $radius1 = $value['current_radius'];
                    $lat11 = $lat - ($radius1 / 111);
                    $lat21 = $lat + ($radius1 / 111);
                    $lon11 = $lon - $radius1 / abs(cos(deg2rad($lat)) * 111);
                    $lon21 = $lon + $radius1 / abs(cos(deg2rad($lat)) * 111);

                    if (!( $value['order_lat'] > $lat11 && $value['order_lat'] < $lat21 && $value['order_lon'] > $lon11 && $value['order_lon'] < $lon21)) {
                        $filter = false;
                        $this->ResponseData["filter_err"] = 'order_lat';
                    }

                    if ($value['order_verify'] == 0) {
                        $filter = false;
                        $this->ResponseData["filter_err"] = 'order_verify';
                    }

                    if ($value['order_condition'] == 1) {
                        if ( !$this->Taxi["taxi_condition"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'order_condition';
                        }
                    }

                    if ($value['order_child'] == 1) {
                        if ( !$this->Taxi["taxi_child"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'taxi_child';
                        }
                    }
                    if ($value['order_nosmoking'] == 1) {
                        if ( !$this->Taxi["taxi_nosmoking"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'order_nosmoking';
                        }
                    }

                    if ($value['order_class'] == 1) {
                        if ( !$this->Taxi["taxi_class_standard"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'taxi_class_standard';
                        }
                    }elseif ($value['order_class'] == 2) {
                        if ( !$this->Taxi["taxi_class_comfort"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'taxi_class_comfort';
                        }
                    }elseif ($value['order_class'] == 3) {
                        if ( !$this->Taxi["taxi_class_business"] == 1) {
                            $filter = false;
                            $this->ResponseData["filter_err"] = 'taxi_class_business';
                        }
                    }


                    if ($this->Taxi["taxi_banned"] == 1) {
                        $filter = false;
                        $this->ResponseData["filter_err"] = 'taxi_banned';
                    }

                    if ($this->TaxiLiderBusy == 1) {
                        $filter = false;
                        $this->ResponseData["filter_err"] = 'taxi_busy';
                    }

                    if ($filter) {
                        $orders[0]["id"] = $value["order_id"];
                        $orders[0]["order_status"] = $value["order_status"];
                        $orders[0]["order_status_date"] = $value["order_status_date"];
                        $orders[0]["order_status_info"] = $value["order_status_info"];
                        $orders[0]["lat"] = $value["order_lat"];
                        $orders[0]["lon"] = $value["order_lon"];
                        $orders[0]["first_name"] = $value["client_name"];
                        $orders[0]["last_name"] = $value["client_last_name"];
                        $orders[0]["phone"] = $value["client_phone"];
                        $orders[0]["response_time"] = 10;
                        $orders[0]["street"] = $value["order_street"];
                        $orders[0]["house"] = $value["order_house"];
                        $orders[0]["building"] = $value["order_building"];
                        $orders[0]["corpus"] = $value["order_corpus"];
                        $orders[0]["entrance"] = $value["order_entrance"];
                        $orders[0]["street"] = $value["order_street"];
                        $orders[0]["destination"] = $value["order_destination"];
                        $orders[0]["drive_time"] = 0;
                        $orders[0]["rating"] = round($value["rating"], 1);
                        $orders[0]["time"] = $value["order_time"] ? date("Y-m-j H:i:s", $value["order_time"]) : "";
                        $orders[0]["child"] = $value["order_child"] ? true : false;
                        $orders[0]["nosmoking"] = $value["order_nosmoking"] ? true : false;
                        $orders[0]["condition"] = $value["order_condition"] ? true : false;

                        if ($value['order_class'] == 1 ) $class_auto = 'standard';
                        if ($value['order_class'] == 2 ) $class_auto = 'comfort';
                        if ($value['order_class'] == 3 ) $class_auto = 'business';
                        $orders[0]["type"] = $class_auto;

                        if ($this->ExceptionTaxiStatus($value["order_id"], $this->Taxi["taxi_id"], $value['order_time'] , 1)) {
                            $exception_order_id = $value["order_id"];
                            $exception_taxi_id = $this->Taxi["taxi_id"];
                            $time = time();
                            $sql_add_exception_taxi_id = "INSERT INTO order_cash
							(order_id, taxi_id, time)
						VALUES
							($exception_order_id, $exception_taxi_id, $time)";
                            $db->sql_query($sql_add_exception_taxi_id);
                        }
                        break;
                    }
                }
            }
        }



        if (count($orders))


            //    $this->ResponseData["token"] = $token;
            $this->ResponseData["taxi_info"] = $taxi_info;
        $this->ResponseData["tariff"] = $tariff;
        $this->ResponseData["orders"] = $orders;

        return true;
    }

    public function ExceptionTaxiStatus($order_id, $taxi_id, $order_time, $type) {

        global $db, $settings;

        $time = time();
        $time_ = $time-($settings['show_order_time']-3);
        $array_length = 0;

        if ($type == 0) {
            $max_taxi = $settings['count_on_step'];

            $sql_exception_count = "SELECT *
						FROM order_cash
						WHERE order_id = {$order_id} AND time > {$time_}";


            if($result_exception = $db->sql_query($sql_exception_count)) {

                if($db->sql_numrows($result_exception)) {

                    $taxiArr = $db->sql_fetchrowset($result_exception);
                    $db->sql_freeresult($result_exception);
                    $array_length = count($taxiArr);
                }
            }
            $this->ResponseData["array_length"] = $array_length;
            $sql_exception_taxi = "SELECT *
						FROM order_cash
						WHERE order_id = {$order_id} AND taxi_id = {$taxi_id}";

            if ($array_length < $max_taxi) {
                if($result_exception = $db->sql_query($sql_exception_taxi)) {

                    if($db->sql_numrows($result_exception)) {

                        $taxiArr = $db->sql_fetchrowset($result_exception);
                        $db->sql_freeresult($result_exception);

                        if (($taxiArr[0]['time']+$settings['show_order_time']) < $time) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        elseif ($type == 1) {

            $sql_exception_taxi = "SELECT *
						FROM order_cash
						WHERE order_id = {$order_id} AND taxi_id = {$taxi_id}";

            if($result_exception = $db->sql_query($sql_exception_taxi)) {

                if($db->sql_numrows($result_exception)) {
                    return false;
                }
                else {
                    return true;
                }

            }
        }
    }

    public function GetActiveOrder()
    {
        global $db;

        $sql = "SELECT *
						FROM ".TABLE_ORDERS."
						WHERE taxi_id = {$this->Taxi["taxi_id"]} AND
									order_status = 3
						ORDER BY order_date DESC
						LIMIT 1";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $order = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            return $order;
        }

        return "";
    }

    public function CalculateOrder($orderId, $lat, $lon)
    {
        global $db, $rootPath;

        $orderInfo = $this->GetOrderInfo($orderId);

        require_once($rootPath.INC_DIR."class.tariffs.php");
        $tariffs = new Tariffs();
        $orderTariff = $tariffs->GetItem($orderInfo["tariff_id"]);

        $lastLat = $orderInfo["lat"];
        $lastLon = $orderInfo["lon"];

        $distance = $orderInfo["distance"];
        $delay = $orderInfo["delay"];
        $cost = $orderInfo["cost"];
        $dateMove = $orderInfo["last_date_move"];
        //echo $lat." - ".$lon." || ";
        //echo $lastLat." - ".$lastLon;
        if($lastLat == $lat && $lastLon == $lon)
        {
            //$delayDiff = (time() - $orderInfo["last_date"]);
            //$delay = $delay + $delayDiff;
            //$cost = $orderInfo["cost"] + ($delayDiff * $orderTariff["tariff_delay_cost"] / 60);
        }
        else
        {
            $distanceDiff = GeoDistance($lastLat, $lastLon, $lat, $lon);
            $distance = $distance + $distanceDiff;
            $cost = $orderInfo["cost"] + ($distanceDiff * $orderTariff["tariff_distance_cost"]);
            $dateMove = time();
        }

        $sql = "UPDATE ".TABLE_ORDERS_INFO."
						SET lat = {$lat},
								lon = {$lon},
								distance = {$distance},
								delay = {$delay},
								cost = {$cost},
								last_date = ".time().",
								last_date_move = {$dateMove}
						WHERE order_id = {$orderId}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        return true;
    }

    public function GetOrderInfo($orderId)
    {
        global $db;

        $sql = "SELECT *
						FROM ".TABLE_ORDERS_INFO."
						WHERE order_id = {$orderId}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $orderInfo = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            return $orderInfo;
        }

        return "";
    }

    public function GetNearCars()
    {
        global $db, $settings;

        /*if(!isset($_POST["lat"])) { $this->SetError(21, "Отсутствует параметр lat"); return false; }
        if(!isset($_POST["lon"])) { $this->SetError(22, "Отсутствует параметр lon"); return false; }

        if(empty($_POST["lat"])) { $this->SetError(23, "Параметр lat пустой"); return false; }
        if(empty($_POST["lon"])) { $this->SetError(24, "Параметр lon пустой"); return false; }

        $lat = $_POST["lat"];
        $lon = $_POST["lon"];

        $radius = $settings["radius"];
        $taxiArr = $this->SearchTaxiBySquare($lat, $lon, $radius, $this->Taxi["taxi_id"]);*/

        if(!isset($_POST["top_lat"])) { $this->SetError(21, "Отсутствует параметр top_lat"); return false; }
        if(!isset($_POST["top_lon"])) { $this->SetError(22, "Отсутствует параметр top_lon"); return false; }
        if(!isset($_POST["bot_lat"])) { $this->SetError(21, "Отсутствует параметр bot_lat"); return false; }
        if(!isset($_POST["bot_lon"])) { $this->SetError(22, "Отсутствует параметр bot_lon"); return false; }

        if(empty($_POST["top_lat"])) { $this->SetError(23, "Параметр top_lat пустой"); return false; }
        if(empty($_POST["top_lon"])) { $this->SetError(24, "Параметр top_lon пустой"); return false; }
        if(empty($_POST["bot_lat"])) { $this->SetError(23, "Параметр bot_lat пустой"); return false; }
        if(empty($_POST["bot_lon"])) { $this->SetError(24, "Параметр bot_lon пустой"); return false; }

        $lat1 = $_POST["top_lat"];
        $lon1 = $_POST["top_lon"];
        $lat2 = $_POST["bot_lat"];
        $lon2 = $_POST["bot_lon"];

        $taxiArr = $this->SearchTaxiByRectangle($lat1, $lon1, $lat2, $lon2, $this->Taxi["taxi_id"]);

        $cars = array();

        if($taxiArr)
        {
            foreach($taxiArr as $key => $value)
            {
                $cars[$key]["id"] = $value["taxi_id"];
                $cars[$key]["lat"] = $value["lat"];
                $cars[$key]["lon"] = $value["lon"];
            }
        }

        $this->ResponseData["cars"] = $cars;

        return true;
    }

    public function SearchTaxiBySquare($lat, $lon, $distance, $excludeId = false)
    {
        global $db, $settings;

        // 1° of latitude ~= 111 km
        // 1° of longitude ~= cos(latitude) * 111

        $where = $excludeId ? " AND taxi_id <> ".$excludeId : "";
        $timeout = time() - $this->TaxiActivityTimeout;

        $lat1 = $lat - ($distance / 111);
        $lat2 = $lat + ($distance / 111);
        $lon1 = $lon - $distance / abs(cos(deg2rad($lat)) * 111);
        $lon2 = $lon + $distance / abs(cos(deg2rad($lat)) * 111);

        $sql = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_on = 1 AND
									taxi_last_activity > {$timeout} AND
									lat BETWEEN {$lat1} AND {$lat2} AND
									lon BETWEEN {$lon1} AND {$lon2}
									{$where}
						GROUP BY taxi_id";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $taxiArr = $db->sql_fetchrowset($result);
            $db->sql_freeresult($result);
            return $taxiArr;
        }

        return "";
    }

    public function SearchTaxiByRectangle($lat1, $lon1, $lat2, $lon2, $excludeId = false)
    {
        global $db, $settings;

        $where = $excludeId ? " AND taxi_id <> ".$excludeId : "";
        $timeout = time() - $this->TaxiActivityTimeout;

        $sql = "SELECT *
						FROM {$this->DbTable}
						WHERE taxi_on = 1 AND
									taxi_last_activity > {$timeout} AND
									lat BETWEEN {$lat2} AND {$lat1} AND
									lon BETWEEN {$lon1} AND {$lon2}
									{$where}
						GROUP BY taxi_id";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $taxiArr = $db->sql_fetchrowset($result);
            $db->sql_freeresult($result);
            return $taxiArr;
        }

        return "";
    }

    public function SearchTaxiByRadius($lat, $lon, $radius, $excludeId = false)
    {
        global $db, $settings;

        $where = $excludeId ? " AND taxi_id <> ".$excludeId : "";
        $timeout = time() - $this->TaxiActivityTimeout;

        $lat1 = $lat - ($radius / 111);
        $lat2 = $lat + ($radius / 111);
        $lon1 = $lon - $radius / abs(cos(deg2rad($lat)) * 111);
        $lon2 = $lon + $radius / abs(cos(deg2rad($lat)) * 111);

        //$sql = "SELECT *, 6371 * 2 * ASIN(SQRT(POWER(SIN(({$lat} - lat) *  pi()/180 / 2), 2) + COS({$lat} * pi()/180) * COS(lat * pi()/180) * POWER(SIN(({$lon} - lon) * pi()/180 / 2), 2) )) AS distance
        $sql = "SELECT *, 6371 * ACOS(COS(RADIANS(lat)) * COS(RADIANS({$lat})) * COS(RADIANS({$lon}) - RADIANS(lon)) + SIN(RADIANS(lat)) * SIN(RADIANS({$lat}))) AS distance
						FROM
						WHERE taxi_on = 1 AND
									taxi_last_activity > {$timeout} AND
									lat BETWEEN {$lat1} AND {$lat2} AND
									lon BETWEEN {$lon1} AND {$lon2}
									{$where}
						HAVING distance < {$radius}
						ORDER BY distance";

        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $taxiArr = $db->sql_fetchrowset($result);
            $db->sql_freeresult($result);
            return $taxiArr;
        }

        return "";
    }

    public function GetRegistrationIds($idsArray)
    {
        global $db;

        $idsString = implode(",", $idsArray);

        $sql = "SELECT taxi_id, registration_id
						FROM {$this->DbTable}
						WHERE taxi_id IN ({$idsString})";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записей", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $rows = $db->sql_fetchrowset($result);
            $db->sql_freeresult($result);
            return $rows;
        }

        return "";
    }

    public function AcceptOrder()
    {
        global $db, $rootPath;
        require_once($rootPath.'/includes/class.orders.php');

        if(!isset($_GET["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_GET["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_GET["id"]);

        require_once($rootPath.INC_DIR."class.orders.php");
        $ordersObj = new Orders();

        $orderToAccept = $ordersObj->GetItem($id);

        // if ($orderToAccept['taxi_id'] != $this->Taxi['taxi_id']) { $this->SetError(123, "Заказ принят другим таксистом"); return false; }


        if(!$orderToAccept) { $this->SetError(53, "Заказ не найден"); return false; }
        if($orderToAccept["taxi_id"]) { $this->SetError(54, "Заказ уже принят другим Такси"); return false; }
        if($orderToAccept["order_status"] == 6) { $this->SetError(55, "Заказ был отменен"); return false; }

        $sql = "UPDATE ".TABLE_ORDERS."
						SET taxi_id = {$this->Taxi["taxi_id"]},
								order_status = 1,
								order_status_date = ".time()."
						WHERE order_id = {$id}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        $sql = "UPDATE " . TABLE_TAXI_MASTER_CONNECTION . " SET taken_by_own_user = 1 WHERE own_order_id = '{$id}'";
        if (!$result = $db->sql_query($sql)) {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        //ExecuteScript("http://".$_SERVER["SERVER_NAME"]."/apns.php", array('a' => 'taxi_accept', 'id' => $id));

        require_once($rootPath.INC_DIR."class.apns.php");
        $apns = new Apns();
        ob_start();
        $apns->TaxiAccept($id);
        ob_get_clean();

        /*require_once($rootPath.INC_DIR."class.gcm_operations.php");
        $gcm = new GCMOperations();
        $recipientsIdsString = $gcm->GetGCMRecipients("new_order", $id);

        if($recipientsIdsString)
        {
            $recipientsIdsArray = explode(",", $recipientsIdsString);
            if( ($key = array_search($this->Taxi["taxi_id"], $recipientsIdsArray)) !== false ) unset($recipientsIdsArray[$key]);
            $registrationIdsArr = $this->GetRegistrationIds($recipientsIdsArray);
            $registrationIds = GetArrayOfFields($registrationIdsArr, "registration_id");

            $messageData["srv_msg"]["type"] = "order_remove";
            $messageData["srv_msg"]["data"]["id"] = $id;

            $GoogleResponse = $gcm->SendGCMRequest($registrationIds, $messageData);
            $gcm->SaveGCMResponse($GoogleResponse, 'order_remove', $id, implode(",", $recipientsIdsArray));
        }*/
        $sql_delete_order_cash = "DELETE FROM order_cash WHERE order_id = {$id}";
        if (!$db->sql_query($sql_delete_order_cash)) {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
        }
        return true;
    }

    public function CreateOrderInfo($orderId, $lat, $lon)
    {
        global $db, $rootPath;

        require_once($rootPath.INC_DIR."class.tariffs.php");
        $tariffs = new Tariffs();
        $orderTariff = $tariffs->GetItem(1);

        $sql = "INSERT INTO ".TABLE_ORDERS_INFO."
							(order_id, lat, lon, cost, last_date, last_date_move, tariff_id)
						VALUES
							($orderId, {$lat}, {$lon}, 0, ".time().", ".time().", 1)";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError("Ошибка при создании записи", __FILE__, __LINE__, $db->sql_error(), false, false);
            return false;
        }

        return true;
    }

    public function SetOrderCost()
    {
        global $db, $rootPath;

        if(!isset($_POST["order_id"])) { $this->SetError(51, "Отсутствует параметр order_id"); return false; }
        if(!isset($_POST["cost"])) { $this->SetError(51, "Отсутствует параметр cost"); return false; }

        $orderId = intval($_POST["order_id"]);
        $cost = intval($_POST["cost"]);

        require_once($rootPath.INC_DIR."class.orders.php");
        $ordersObj = new Orders();

        $order = $ordersObj->GetItem($orderId);
        if(!$order) { $this->SetError(53, "Заказ не найден"); return false; }

        $sql = "UPDATE ".TABLE_ORDERS_INFO."
						SET cost = {$cost},
								last_date = ".time()."
						WHERE order_id = {$orderId}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        //$orderInfo = $this->GetOrderInfo($id);
        //$this->ResponseData["cost"] = $orderInfo["cost"];

        return true;
    }

    public function GetOrderCost()
    {
        global $db, $rootPath;

        if(!isset($_GET["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_GET["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_GET["id"]);

        require_once($rootPath.INC_DIR."class.orders.php");
        $ordersObj = new Orders();

        $order = $ordersObj->GetItem($id);

        if(!$order) { $this->SetError(53, "Заказ не найден"); return false; }

        $orderInfo = $this->GetOrderInfo($id);
        $this->ResponseData["cost"] = $orderInfo["cost"];
        $this->ResponseData["distance"] = $orderInfo["distance"];
        $this->ResponseData["time"] = $orderInfo["last_date_move"] - $order['order_date'];

        return true;
    }

    public function TaxiArrive()
    {
        global $db, $rootPath;
        require_once($rootPath.'/includes/class.orders.php');

        if(!isset($_GET["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_GET["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_GET["id"]);

        $ordersObj = new Orders();
        $order = $ordersObj->GetItem($id);

        if ($order['taxi_id'] != $this->Taxi['taxi_id']) { $this->SetError(123, "Заказ принят другим таксистом"); return false; }

        $orderToAccept = $ordersObj->GetItem($id);

        if(!$orderToAccept) { $this->SetError(53, "Заказ не найден"); return false; }
        if($orderToAccept["order_status"] == 6) { $this->SetError(55, "Заказ был отменен"); return false; }

        $sql = "UPDATE ".TABLE_ORDERS."
						SET order_status = 2,
								order_status_date = ".time()."
						WHERE order_id = {$id}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        //ExecuteScript("http://".$_SERVER["SERVER_NAME"]."/apns.php", array('a' => 'taxi_arrive', 'id' => $id));

        /*$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER["SERVER_NAME"]."/apns.php?a=taxi_arrive&id=".$id);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);

        curl_exec($ch);
        curl_close($ch);*/

        require_once($rootPath.INC_DIR."class.apns.php");
        $apns = new Apns();
        ob_start();
        $apns->TaxiArrive($id);
        ob_get_clean();

        /*if (substr(php_uname(), 0, 7) == "Windows")
        {
            pclose(popen("start /B ". "/apns.php?a=taxi_arrive&id=".$id, "r"));
        }
        else
        {
            exec($_SERVER["DOCUMENT_ROOT"]."/apns.php?a=taxi_arrive&id=".$id." > /dev/null &");
        }*/

        return true;
    }

    public function StartDrive()
    {
        global $db, $rootPath;
        require_once($rootPath.'/includes/class.orders.php');


        if(!isset($_GET["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_GET["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_GET["id"]);
        $order = new Orders();
        $order = $order->GetItem($id);

        if ($order['taxi_id'] != $this->Taxi['taxi_id']) { $this->SetError(123, "Заказ принят другим таксистом"); return false; }

        $sql = "UPDATE ".TABLE_ORDERS."
						SET order_status = 3,
								order_status_date = ".time()."
						WHERE order_id = {$id}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        $this->CreateOrderInfo($id, $this->Taxi["lat"], $this->Taxi["lon"]);

        return true;
    }

    public function FinishOrder()
    {
        global $db, $rootPath;
        require_once($rootPath.'/includes/class.orders.php');

        if(!isset($_GET["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_GET["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_GET["id"]);

        $order = new Orders();
        $order = $order->GetItem($id);

        if ($order['taxi_id'] != $this->Taxi['taxi_id']) { $this->SetError(123, "Заказ принят другим таксистом"); return false; }

        $sql = "UPDATE ".TABLE_ORDERS."
						SET order_status = 4,
								order_status_date = ".time()."
						WHERE order_id = {$id}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_affectedrows())
        {
            $sql_order_info = "SELECT * FROM `orders_info` WHERE order_id = {$id}";

            if(!$result = $db->sql_query($sql_order_info))
            {
                $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
                return false;
            }
            elseif($db->sql_numrows($result))
            {
                $taxi_id = $this->Taxi['taxi_id'];
                $cost = $db->sql_fetchrow($result);
                $db->sql_freeresult($result);

                if ($this->Taxi['admin_id'] != 25) {
                    $new_balance = ($cost['cost']/100)*5;
                    $new_balance = $this->Taxi['taxi_balance'] - $new_balance;
                    $sql_set_new_balance = "UPDATE `taxi` SET `taxi_balance`={$new_balance} WHERE taxi_id={$taxi_id}";
                }
                /////////////////////////////

                if(!$result = $db->sql_query($sql_set_new_balance))
                {
                    $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
                    return false;
                }
                elseif($db->sql_affectedrows())
                {
                    return true;
                }
            }


        }

        return "";
    }

    public function CancelOrder()
    {
        global $db, $rootPath;
        require_once($rootPath.'/includes/class.orders.php');

        if(!isset($_POST["id"])) { $this->SetError(51, "Отсутствует параметр id"); return false; }
        if(empty($_POST["id"])) { $this->SetError(52, "Параметр id пустой"); return false; }

        $id = intval($_POST["id"]);
        $comment = isset($_POST["comment"]) ? $_POST["comment"] : "";

        $order = new Orders();
        $order = $order->GetItem($id);

        if ($order['taxi_id'] != $this->Taxi['taxi_id']) { $this->SetError(123, "Заказ принят другим таксистом"); return false; }


        $sql = "UPDATE ".TABLE_ORDERS."
						SET order_status = 5,
								order_status_info = '{$comment}',
								order_status_date = ".time()."
						WHERE order_id = {$id}";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при обновлении записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        //ExecuteScript("http://".$_SERVER["SERVER_NAME"]."/apns.php", array('a' => 'taxi_cancel', 'id' => $id));

        require_once($rootPath.INC_DIR."class.apns.php");
        $apns = new Apns();
        ob_start();
        $apns->TaxiCancel($id);
        ob_get_clean();

        return "";
    }

    public function GetTaxiLastLocation($taxiId)
    {
        global $db;

        $sql = "SELECT *
						FROM ".TABLE_TAXI_LOCATION."
						WHERE taxi_id = {$taxiId}
						ORDER BY date DESC
						LIMIT 1";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при выборе записи", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }
        elseif($db->sql_numrows($result))
        {
            $location = $db->sql_fetchrow($result);
            $db->sql_freeresult($result);
            return $location;
        }

        return "";
    }

    public function RateClient()
    {
        global $db;

        if(!isset($_POST["client_id"])) { $this->SetError(60, "Отсутствует параметр client_id"); return false; }
        if(!isset($_POST["order_id"])) { $this->SetError(61, "Отсутствует параметр order_id"); return false; }
        if(!isset($_POST["rating"])) { $this->SetError(62, "Отсутствует параметр rating"); return false; }

        $clientId = $_POST["client_id"];
        $orderId = $_POST["order_id"];
        $rate = $_POST["rating"];
        $comment = isset($_POST["comment"]) ? $_POST["comment"] : "";

        $sql = "INSERT INTO ".TABLE_CLIENTS_REVIEWS."
							(taxi_id, client_id, order_id, rate_number, rate_comment, rate_date)
						VALUES
							({$this->Taxi['taxi_id']}, {$clientId}, {$orderId}, {$rate}, '{$comment}', ".time().")";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError(1, "Ошибка при записи отзыва", false, __FILE__, __LINE__, $db->sql_error());
            return false;
        }

        return true;
    }

    public function Response()
    {
        $response["status"]["code"] = $this->StatusCode;
        $response["status"]["message"] = $this->StatusMessage;

        if($this->ResponseData)
        {
            $response["response"]["data"] = $this->ResponseData;
        }

        $response["response"]["error"]["code"] = $this->ErrorCode;
        $response["response"]["error"]["message"] = $this->ErrorMessage;

        echo json_encode($response);
    }

    public function SetError($code, $error = "", $valid = false, $file = false, $line = false, $sql = false)
    {
        if(!$valid) $this->Valid = $valid;

        $errorText = "";
        if($error) $errorText .= $error;
        if($file)  $errorText .= " | File: ".$file;
        if($line)  $errorText .= " | Line: ".$line;
        if($sql)   $errorText .= " | SQL Error #".$sql["code"].": ".$sql["message"];
        $errorText .= "";

        $this->ErrorCode = $code;
        $this->ErrorMessage = $errorText;

        $this->WriteLog(2, $code." - ".$error, $file, $line, $sql);

        return true;
    }

    public function WriteLog($status, $text, $file = false, $line = false, $sql = false)
    {
        global $db;

        $phpSelf    = $_SERVER['PHP_SELF'];
        $text       = $db->sql_escape($text);
        $file       = $file ? $db->sql_escape($file) : "";
        $line       = $line ? $line : 0;
        $sqlCode    = $sql ? $db->sql_escape($sql["code"]) : "";
        $sqlMessage = $sql ? $db->sql_escape($sql["message"]) : "";
        $date       = time();

        $sql = "INSERT INTO ".TABLE_LOG."
							(log_status, log_php_self, log_text, log_file, log_line, log_sql_code, log_sql_message, log_date)
						VALUES
							($status, '$phpSelf', '$text', '$file', $line, '$sqlCode', '$sqlMessage', $date)";
        if(!$result = $db->sql_query($sql))
        {
            $this->SetError("Ошибка при логгировании", __FILE__, __LINE__, $db->sql_error(), false, false);
            return false;
        }

        return true;
    }

}

?>
