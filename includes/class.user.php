<?php

class User {

    public $user_id = 0;
    public $user_login = '';
    public $user_pass = '';
    public $user_type = '';
    public $user_token = '';
    public $user_name = '';
    public $user_last_name = '';
    public $user_last_id = 0;
    public $user_patronymic = '';

    public function __construct()
    {

    }

    public function init($token) {

        global $db;

        $sql = "SELECT * FROM users WHERE user_token = '".$token."'";

        $result = $db->sql_query($sql);

        if ($row = $db->sql_fetchrow($result)) {
            $this->user_id = $row['user_id'];
            $this->user_login = $row['user_login'];
            $this->user_token = $row['user_token'];
            $this->user_pass = $row['user_pass'];
            $this->user_type = $row['user_type'];
            $this->user_name = $row['user_name'];
            $this->user_last_name = $row['user_last_name'];
            $this->user_patronymic = $row['user_patronymic'];
            $this->user_last_id = $row['user_last_id'];

            return true;
        }

        return false;

    }

    public function login($login, $pass) {

        global $db;

        $sql = "SELECT * FROM users WHERE user_login = '".$login."' AND user_pass = '".$pass."'";

        $result = $db->sql_query($sql);

        if ($row = $db->sql_fetchrow($result)) {
            $this->user_id = $row['user_id'];
            $this->user_login = $row['user_login'];
            $this->user_token = $row['user_token'];
            $this->user_pass = $row['user_pass'];
            $this->user_type = $row['user_type'];
            $this->user_name = $row['user_name'];
            $this->user_last_name = $row['user_last_name'];
            $this->user_patronymic = $row['user_patronymic'];
            $this->user_last_id = $row['user_last_id'];


        }


    }

    public function user_json() {

        $result = array();

        $result['user_name'] = $this->user_name;
        $result['user_last_name'] = $this->user_last_name;
        $result['user_type'] = $this->user_type;
        $result['user_token'] = $this->user_token;
        $result['user_last_id'] = $this->user_last_id;

        return $result;
    }

    public function user_get_all_json() {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM users";


        if($result_exception = $db->sql_query($sql_exception_count)) {

            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function update() {

        global $db;

        $sql = "UPDATE users
						SET user_id = '{$this->user_id}',
						user_login = '{$this->user_login}',
						user_pass = '{$this->user_pass}',
						user_type = '{$this->user_type}',
						user_name = '{$this->user_name}',
						user_last_name = '{$this->user_last_name}',
						user_patronymic = '{$this->user_patronymic}',
						user_last_id = '{$this->user_last_id}'
						WHERE user_id = '{$this->user_id}'";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
    }

    public function newUser() {

        global $db;

        $token = md5(uniqid(mt_rand(), true));

        $sql = "INSERT INTO users (user_token) VALUES ('".$token."')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            $this->init($token);
        }
    }
    public function deleteUser($token) {

        global $db;

        $sql = "DELETE FROM users WHERE user_token = '".$token."'";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            $this->init($token);
        }
    }


    public function setParams() {

         $this->user_login = $_POST['user_login'];
         $this->user_pass = MD5($_POST['user_pass']);
         $this->user_type = $_POST['user_type'];
         $this->user_token = $_POST['user_type'];
         $this->user_name = $_POST['user_name'];
         $this->user_last_name = $_POST['user_last_name'];
         $this->user_patronymic = $_POST['user_patronymic'];

    }
    public function getStages($id = 0) {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM user_rel LEFT JOIN stage ON user_rel.stage_id = stage.stage_id WHERE user_rel.user_id = ".$id;

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function removeStage($id = 0, $user_id = 0) {

        global $db;
        $result = array();

        $sql_exception_count = "DELETE
						FROM user_rel WHERE id = ".$id;

        if($result_exception = $db->sql_query($sql_exception_count)) {

        }


        $sql = "SELECT *
						FROM user_rel LEFT JOIN stage ON user_rel.stage_id = stage.stage_id WHERE user_rel.user_id = ".$user_id;

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }


        return $result;
    }

    public function addStage($user_id,$stage_id) {

        global $db;
        $result = array();


        $sql = "INSERT INTO `user_rel`(`user_id`, `stage_id`) VALUES ('$user_id','$stage_id')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }


        $sql = "SELECT *
						FROM user_rel LEFT JOIN stage ON user_rel.stage_id = stage.stage_id WHERE user_rel.user_id = ".$user_id;

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function checkStage($stage_id) {

        global $db;

        $user_id = $this->user_id;

        $sql = "SELECT *
						FROM user_rel WHERE stage_id = $stage_id AND user_id = $user_id";

        if($res = $db->sql_query($sql)) {


            if($db->sql_numrows($res)) {

                return true;
            }
            else {
                return false;
            }
        }

        return false;

    }

}

?>