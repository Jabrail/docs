<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 9:13
 */

class Newspaper {

    public $id = 0;
    public $name = '';
    public $number = '';
    public $date = '';
    public $font = '';
    public $db_name = 'newspapers';
    public function __construct()
    {

    }

    public function init($id = 0) {

        global $db;

        $sql = "SELECT * FROM $this->db_name WHERE id = $id";

        $result = $db->sql_query($sql);

        if ($row = $db->sql_fetchrow($result)) {

            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->number = $row['number'];
            $this->date = $row['date'];
            $this->font = $row['font'];

        }

    }

    public function getAll() {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM $this->db_name ";

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function get($id) {

        global $db;
        $result = array();

        $sql_exception_count = "SELECT *
						FROM $this->db_name WHERE id = $id";

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function getContent() {

        global $db;
        $result = array();
        $id  = $this->id;
        $sql_exception_count = "SELECT *
						FROM content WHERE n_id = $id";

        if($result_exception = $db->sql_query($sql_exception_count)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function removeStage($id = 0, $id = 0) {

        global $db;
        $result = array();

        $sql_exception_count = "DELETE
						FROM type_rel WHERE id = ".$id;

        if($result_exception = $db->sql_query($sql_exception_count)) {

        }


        $sql = "SELECT *
						FROM type_rel LEFT JOIN stage ON type_rel.stage_id = stage.stage_id WHERE type_rel.id = ".$id;

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }


        return $result;
    }

    public function addContent($content , $label, $count, $checked) {

         global $db;
        $result = array();

        $id = $this->id;

        $checked = $checked == 'true' ? 1 : 0;
        $sql = "INSERT INTO `content` ( `n_id`, `label`, `text`, `col`, `checked`) VALUES ('$id','$label','$content', '$count', '$checked')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }


        $sql = "SELECT *
						FROM type_rel LEFT JOIN stage ON type_rel.stage_id = stage.stage_id WHERE type_rel.id = ".$id;

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function editContent($cid, $content , $label, $count, $checked) {

         global $db;
        $result = array();

        $id = $this->id;

        $checked = $checked == 'true' ? 1 : 0;

        $sql = "UPDATE `content` SET
                                      `label` = '$label',
                                      `text` = '$content',
                                      `col` = '$count',
                                      `checked` = '$checked'

                                  WHERE id = '$cid' ";

         if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }


        $sql = "SELECT *
						FROM type_rel LEFT JOIN stage ON type_rel.stage_id = stage.stage_id WHERE type_rel.id = ".$id;

        if($result_exception = $db->sql_query($sql)) {


            if($db->sql_numrows($result_exception)) {

                $result = $db->sql_fetchrowset($result_exception);
                $db->sql_freeresult($result_exception);
            }
        }

        return $result;
    }

    public function update() {

        global $db;

        $sql = "UPDATE {$this->db_name}
						SET `name` = '{$this->name}',
						    `number` = '{$this->number}',
						    `font` = '{$this->font}',
						    `date` = '{$this->date}'
						WHERE id = '{$this->id}'";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }

    }

    public function create() {

        global $db;

        $date = time();
        $sql = "INSERT INTO $this->db_name (name) VALUES ('')";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            $this->init($db->sql_nextid());
        }
    }

    public function remove($id) {

        global $db;

        $sql = "DELETE FROM $this->db_name WHERE id = $id";

        if(!$result = $db->sql_query($sql)) {
            var_dump($db->sql_error($sql));
        }
        else {
            return true;
        }
    }
}

?>