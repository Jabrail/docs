<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 5:11
 */

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/newspaper.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;



if ($action == 'add') {
    $newspaper = new Newspaper();
    $newspaper->create();
    $newspaper->name = isset($_GET['name']) ? $_GET['name'] : '';
    $newspaper->date = $_GET['date'];
    $newspaper->number = $_GET['number'];
    $newspaper->font = $_GET['font'];
    $newspaper->update();

    $response = $newspaper->getAll();
    echo json_encode($response);

}
if ($action == 'edit') {
    $newspaper = new Newspaper();
    $newspaper->init($_GET['id']);
    $newspaper->name = isset($_GET['name']) ? $_GET['name'] : '';
    $newspaper->date = $_GET['date'];
    $newspaper->font = $_GET['font'];
    $newspaper->number = $_GET['number'];
    $newspaper->update();

    $response = $newspaper->getAll();
    echo json_encode($response);

} elseif ($action == 'addContent') {


    $newspaper = new Newspaper();
    $newspaper->init($_GET['id']);
    $response = $newspaper->addContent($_POST['text'], $_POST['title'], $_POST['count'], $_POST['title_in']);
    echo json_encode( $newspaper->getContent());

}elseif ($action == 'editContent') {


    $newspaper = new Newspaper();
    $newspaper->init($_GET['id']);
    $newspaper->editContent($_POST['id'],$_POST['text'], $_POST['title'], $_POST['count'], $_POST['title_in']);
    echo json_encode( $newspaper->getContent());

} elseif ($action == 'getContent') {

    $newspaper = new Newspaper();
    $newspaper->init($_GET['id']);
    $response = $newspaper->getContent();
    echo json_encode($response);


}elseif ($action == 'remove') {


    $id = isset($_GET['id']) ? $_GET['id'] : 0;
    $newspaper = new Newspaper();
    $newspaper->remove($id);
    $response = $newspaper->getAll();
    echo json_encode($response);

} elseif ($action == 'get') {

    $newspaper = new Newspaper($_GET['id']);
    $response = $newspaper->get($_GET['id']);
    echo json_encode($response);

} elseif ($action == 'getAll') {

    $newspaper = new Newspaper();
    $response = $newspaper->getAll();
    echo json_encode($response);

}
?>
