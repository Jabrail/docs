-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 20 2014 г., 14:17
-- Версия сервера: 5.6.20
-- Версия PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `pfr_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cases`
--

CREATE TABLE IF NOT EXISTS `cases` (
`case_id` int(11) NOT NULL,
  `case_name` varchar(256) COLLATE utf8_bin NOT NULL,
  `case_last_name` varchar(256) COLLATE utf8_bin NOT NULL,
  `case_patronymic` varchar(256) COLLATE utf8_bin NOT NULL,
  `case_create_date` int(20) NOT NULL,
  `case_date_ex_in_ONIPP` int(20) NOT NULL,
  `case_performer_in_ONIPP` int(11) NOT NULL,
  `case_date_ex_in_CVP` int(20) NOT NULL,
  `case_performer_in_CVP` int(11) NOT NULL,
  `creator_user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=130 ;

--
-- Дамп данных таблицы `cases`
--

INSERT INTO `cases` (`case_id`, `case_name`, `case_last_name`, `case_patronymic`, `case_create_date`, `case_date_ex_in_ONIPP`, `case_performer_in_ONIPP`, `case_date_ex_in_CVP`, `case_performer_in_CVP`, `creator_user_id`, `type_id`, `status`) VALUES
(123, 'Иван', 'Иванов', 'Иванович', 1413491166, 0, 0, 0, 0, 20, 0, 2),
(124, 'Иван', 'Иванов', 'Иванович', 1413491168, 0, 0, 0, 0, 20, 0, 2),
(125, 'Иван', 'Иванов', 'Иванович', 1413491170, 0, 0, 0, 0, 20, 0, 2),
(126, 'Иван', 'Иванов', 'Иванович', 1413491239, 0, 0, 0, 0, 20, 0, 6),
(127, 'Иван', 'Иванов', 'Иванович', 1413491284, 0, 0, 0, 0, 20, 0, 6),
(128, 'Иван', 'Иванов', 'Иванович', 1413491315, 0, 0, 0, 0, 20, 0, 6),
(129, 'Иван', 'Иванов', 'Иванович', 1413491323, 0, 0, 0, 0, 20, 0, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `cvp_first`
--

CREATE TABLE IF NOT EXISTS `cvp_first` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `cvp_first`
--

INSERT INTO `cvp_first` (`id`, `user_id`, `case_id`) VALUES
(1, 18, 119),
(2, 18, 122),
(3, 18, 121),
(4, 18, 120),
(5, 17, 129),
(6, 17, 128),
(7, 17, 127),
(8, 17, 126),
(9, 17, 125),
(10, 17, 124),
(11, 17, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `cvp_two`
--

CREATE TABLE IF NOT EXISTS `cvp_two` (
`CVP_two_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `cvp_two`
--

INSERT INTO `cvp_two` (`CVP_two_id`, `user_id`, `case_id`) VALUES
(1, 20, 129),
(2, 20, 128),
(3, 20, 127),
(4, 20, 126);

-- --------------------------------------------------------

--
-- Структура таблицы `onipp`
--

CREATE TABLE IF NOT EXISTS `onipp` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `onipp`
--

INSERT INTO `onipp` (`id`, `user_id`, `case_id`) VALUES
(1, 19, 0),
(2, 19, 0),
(4, 17, 122),
(6, 0, 121),
(7, 0, 120),
(8, 0, 119),
(9, 19, 129),
(10, 19, 128),
(11, 19, 127),
(12, 19, 126),
(13, 0, 129),
(14, 0, 129),
(15, 0, 125),
(16, 0, 124),
(17, 0, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `package_id` int(11) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
`package_id` int(11) NOT NULL,
  `package_name` varchar(256) COLLATE utf8_bin NOT NULL,
  `creator_user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=86 ;

--
-- Дамп данных таблицы `packages`
--

INSERT INTO `packages` (`package_id`, `package_name`, `creator_user_id`, `status`) VALUES
(38, 'Пакет №38', 0, 0),
(39, 'Пакет №39', 0, 0),
(60, 'Пакет №60', 0, 0),
(61, 'Пакет №61', 0, 0),
(62, '', 0, 0),
(63, '', 0, 0),
(64, '', 0, 0),
(65, '', 0, 0),
(66, '', 0, 0),
(67, '', 0, 0),
(68, '', 0, 0),
(69, '', 0, 0),
(70, '', 0, 0),
(76, 'Пакет №76', 18, 1),
(81, 'Пакет №81', 18, 2),
(82, 'Пакет №82', 18, 2),
(83, 'Пакет №83', 18, 2),
(84, 'Пакет №84', 18, 2),
(85, 'Пакет №85', 20, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `packages_case_rel`
--

CREATE TABLE IF NOT EXISTS `packages_case_rel` (
`p_c_rel_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `packages_case_rel`
--

INSERT INTO `packages_case_rel` (`p_c_rel_id`, `package_id`, `case_id`) VALUES
(1, 37, 69),
(2, 37, 70),
(3, 37, 71),
(4, 37, 72),
(5, 38, 69),
(6, 38, 70),
(7, 38, 71),
(8, 38, 72),
(9, 38, 73),
(10, 39, 69),
(11, 39, 70),
(12, 39, 71),
(13, 39, 72),
(14, 39, 73),
(15, 39, 74),
(16, 40, 69),
(17, 40, 70),
(18, 40, 71),
(19, 40, 72),
(20, 41, 69),
(21, 41, 70),
(22, 41, 71),
(23, 42, 69),
(24, 42, 70),
(25, 42, 71),
(26, 42, 72),
(27, 42, 73),
(28, 43, 91),
(29, 43, 92),
(30, 43, 93),
(31, 43, 94),
(32, 43, 95),
(33, 43, 96),
(34, 43, 97),
(35, 73, 110),
(36, 73, 109),
(37, 74, 110),
(38, 74, 109),
(39, 76, 110),
(40, 76, 109),
(41, 81, 113),
(42, 81, 112),
(43, 81, 111),
(44, 82, 116),
(45, 82, 115),
(46, 82, 114),
(47, 83, 119),
(48, 83, 118),
(49, 83, 117),
(50, 84, 122),
(51, 84, 121),
(52, 84, 120),
(53, 85, 129),
(54, 85, 128),
(55, 85, 127),
(56, 85, 126),
(57, 85, 125),
(58, 85, 124),
(59, 85, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `user_login` varchar(256) NOT NULL,
  `user_pass` varchar(512) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `user_last_name` varchar(256) NOT NULL,
  `user_patronymic` varchar(256) NOT NULL,
  `user_last_id` int(11) NOT NULL,
  `user_token` varchar(512) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_pass`, `user_type`, `user_name`, `user_last_name`, `user_patronymic`, `user_last_id`, `user_token`) VALUES
(15, 'test', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Джабраил', 'Караев', 'К', 0, '444c8460a01b32c5391ab66ddece3a35'),
(17, 'testq', 'e10adc3949ba59abbe56e057f20f883e', 2, 'sdvds', 'sdsd', 'ssdf', 0, '29fe0105abf8c0ce8ab5f701cc8e1b6e'),
(19, 'testo', 'e10adc3949ba59abbe56e057f20f883e', 3, 'sdvvsd', 'sdv', 'sdvsdv', 0, '73e988e76d15b70799484ab4773fd0e4'),
(20, 'testc', 'e10adc3949ba59abbe56e057f20f883e', 4, 'sdvvsd', 'sdv', 'sdvsdv', 0, 'd48c6e649e865db902aea2780c9a285d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cases`
--
ALTER TABLE `cases`
 ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `cvp_first`
--
ALTER TABLE `cvp_first`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cvp_two`
--
ALTER TABLE `cvp_two`
 ADD PRIMARY KEY (`CVP_two_id`);

--
-- Indexes for table `onipp`
--
ALTER TABLE `onipp`
 ADD UNIQUE KEY `ONIPP_id` (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
 ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `packages_case_rel`
--
ALTER TABLE `packages_case_rel`
 ADD PRIMARY KEY (`p_c_rel_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cases`
--
ALTER TABLE `cases`
MODIFY `case_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `cvp_first`
--
ALTER TABLE `cvp_first`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cvp_two`
--
ALTER TABLE `cvp_two`
MODIFY `CVP_two_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `onipp`
--
ALTER TABLE `onipp`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `packages_case_rel`
--
ALTER TABLE `packages_case_rel`
MODIFY `p_c_rel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
