<?php

## Directories - Include
define("INC_DIR",            "includes/");

define("INC_DB",             INC_DIR."db/");
define("INC_SMARTY",         INC_DIR."smarty-3.1.15/");
define("INC_THUMBNAIL",      INC_DIR."thumbnail-1.48/");
define("INC_PHPMAILER",      INC_DIR."phpmailer-5.2.7/");
define("INC_IMAGETRANSFORM", INC_DIR."imagetransform-1.0.5/");

define("INC_JQUERY",         INC_DIR."jquery-2.0.3/");
define("INC_JQUERY_UI",      INC_DIR."jquery-ui-1.10.3/");
define("INC_JQUERY_PLUGINS", INC_DIR."jquery-plugins/");
define("INC_CKEDITOR",       INC_DIR."ckeditor-4.2.2/");

## Directories
define("ADMIN_DIR",     "admin/");
define("ADMIN_TPL_DIR", ADMIN_DIR."tpl/");
define("FILES_DIR",     "files/");

## Directories - Smarty
define("SMARTY_TEMPLATES_DIR", "templates/");
define("SMARTY_COMPILE_DIR",   "temp/");
define("SMARTY_CACHE_DIR",     "cache/");

## Tables
define("TABLE_ADMIN",           "admin");
define("TABLE_LOG",             "log");
define("TABLE_TAXI",            "taxi");
define("TABLE_TAXI_LOCATION",   "taxi_location");
define("TABLE_CLIENTS",         "clients");
define("TABLE_ORDERS",          "orders");
define("TABLE_ORDERS_INFO",     "orders_info");
define("TABLE_SETTINGS",        "settings");
define("TABLE_GCM_RESPONSES",   "gcm_responses");
define("TABLE_TARIFFS",         "tariffs");
define("TABLE_TAXI_REVIEWS",    "taxi_reviews");
define("TABLE_CLIENTS_REVIEWS", "clients_reviews");

// Admin
define("ADMIN_EXPIRED", 3600);

// Global Vriables
define("RADIUS_TAXI_SEARCH", 100000);
define("TAXI_ACTIVITY_TIMEOUT", 10000000);

?>