'use strict';

/* App Module */

var accountingDocsApp = angular.module('accountingDocsApp', [
    'ngRoute',
    'accountingDocsAnimations',

    'accountingDocsControllers',
    'accountingDocsFilters',
    'accountingDocsServices',
    'LocalStorageModule'
], function($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
});

accountingDocsApp.config(['$routeProvider', '$httpProvider',
    function($routeProvider ) {
         $routeProvider.
            when('/newspaper', {
                templateUrl: 'partials/newspaper.html',
                controller: 'TypeCtrl'
            }).
            when('/newspaper/:id', {
                templateUrl: 'partials/newspaper_detail.html',
                controller: 'TypeCurCtrl'
            }).
            otherwise({
                redirectTo: '/newspaper'
            });

    }]);
