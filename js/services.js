'use strict';

/* Services */

var accountingDocsServices = angular.module('accountingDocsServices', ['ngResource']);

accountingDocsServices.factory('Phone', ['$resource',
    function($resource){
        return $resource('phones/:phoneId.json', {}, {
            query: {
                method:'GET',
                params:{phoneId:'phones'},
                isArray:true}
        });
    }]);

angular.module('accountingDocsServices', ['ngResource']).
    factory('Case', function($resource){
        return $resource('case.php', {}, {
            getAllFirst: {method:'GET', params:{first:'1', a: 'get_all', sort: 'sort_by_id'}, isArray:true},
            getAll: {method:'GET', params:{first:'0', a: 'get_all' , sort: 'sort_by_id'}, isArray:true},
            getAllCreate: {method:'GET', params:{first:'0', a: 'get_all_create' , sort: 'sort_by_id'}, isArray:true},
            getAllCVP: {method:'GET', params:{ a: 'get_all_cvp' , sort: 'sort_by_id'}, isArray:true},
            getAllCVPSecond: {method:'GET', params:{ a: 'get_all_cvp_second' , sort: 'sort_by_id'}, isArray:true},
            getAllONIPP: {method:'GET', params:{ a: 'get_all_onipp' , sort: 'sort_by_id'}, isArray:true},
            takeInWorkCVP: {method:'POST', params:{a: 'take_in_work_cvp'}, isArray:true , headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
            sendCVPSecond: {method:'POST', params:{a: 'send_in_cvp_second'}, isArray:true },
            takeInWork: {method:'POST', params:{a: 'take_in_work'}, isArray:true },
            takeInWorkCVPSecond: {method:'POST', params:{a: 'take_in_work_cvp_second'}, isArray:true },
            addCase: {method:'POST', params:{a: 'add_cases'}, isArray:true },
            sendONIPP: {method:'POST', params:{a: 'send_onipp'}, isArray:true },
            complete: {method:'POST', params:{a: 'case_complete'}, isArray:true }
        });
    }).
    factory('Package', function($resource){
        return $resource('package.php', {}, {
            getAllFirst: {method:'GET', params:{first:'1', a: 'get_all', sort: 'sort_by_id'}, isArray:true},
            getAll: {method:'GET', params:{first:'1', a: 'get_all', sort: 'sort_by_id'}, isArray:true},
            getAllCVP: {method:'GET', params:{first:'1', a: 'get_all_cvp', sort: 'sort_by_id'}, isArray:true},
            addPackage: {method:'GET', params:{a: 'add_package'}, isArray:true },
            groupCase: {method:'POST', params:{a: 'group_case'}, isArray:true },
            sendInCVP: {method:'POST', params:{a: 'send_in_cvp'}, isArray:true },
            deletePackage: {method:'POST', params:{a: 'delete_package'}, isArray:true }
        });
    }).
    factory('Stage', function($resource){
        return $resource('stage.php', {}, {
            'add': {method:'POST', params:{a: 'add', sort: 'sort_by_id'}, isArray:true},
            'getStage': {method:'GET', params:{a: 'getStage', sort: 'sort_by_id'}},
            'getAll': {method:'GET', params:{a: 'getAll', sort: 'sort_by_id'}, isArray:true},
            'allForUser': {method:'POST', params:{a: 'allForUser', sort: 'sort_by_id'}, isArray:true},
            'addForUser': {method:'POST', params:{a: 'addForUser', sort: 'sort_by_id'}},
            'removeForUser': {method:'POST', params:{a: 'removeForUser', sort: 'sort_by_id'}},
            'remove': {method:'POST', params:{a: 'remove', sort: 'sort_by_id'}, isArray:true}
        });
    }).
    factory('Newspaper', function($resource){
        return $resource('responder.php', {}, {
            'add': {method:'GET', params:{a: 'add', sort: 'sort_by_id'}, isArray:true},
            'edit': {method:'GET', params:{a: 'edit', sort: 'sort_by_id'}, isArray:true},
            'get': {method:'GET', params:{a: 'get', sort: 'sort_by_id'}, isArray:true},
            'getAll': {method:'GET', params:{a: 'getAll', sort: 'sort_by_id'}, isArray:true},
            'remove': {method:'GET', params:{a: 'remove', sort: 'sort_by_id'}, isArray:true},
            'addContent': {method:'POST', params:{a: 'addContent', sort: 'sort_by_id'}, isArray:true,   headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
            'editContent': {method:'POST', params:{a: 'editContent', sort: 'sort_by_id'}, isArray:true,   headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
            'getContent': {method:'GET', params:{a: 'getContent', sort: 'sort_by_id'}, isArray:true},
            'getStages': {method:'POST', params:{a: 'getStages', sort: 'sort_by_id'}, isArray:true}
        });
    }).
    factory('Work', function($resource){
        return $resource('work.php', {}, {
            'create': {method:'POST', params:{a: 'create', sort: 'sort_by_id'}, isArray:true },
            'take': {method:'POST', params:{a: 'take', sort: 'sort_by_id'}, isArray:true },
            'next': {method:'POST', params:{a: 'next', sort: 'sort_by_id'}, isArray:true}
        });
    }).
    factory('Users', function($resource){
        return $resource('user.php', {}, {
            getAllFirst: {method:'GET', params:{first:'1', a: 'get_all'}, isArray:true},
            getAll: {method:'GET', params:{first:'1', a: 'get_all', sort: 'sort_by_id'}, isArray:true},
            addUser: {method:'POST', params:{a: 'add_user'}, isArray:true },
            deleteUser: {method:'POST', params:{a: 'delete_user'}, isArray:true }
        });
    });

/*
accountingDocsServices.factory('CaseResource', ['$resource',
    function($resource , token){
        return $resource('http://localhost/apps_for_pfr/case.php', {}, {
            query: {
                method:'GET',
                isArray:true
            },
            queryAdd: {
                url: 'http://localhost/apps_for_pfr/case.php?a=add_cases',
                method:'POST',
                isArray:true
            }
        });
    }]);
*/
