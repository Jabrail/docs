'use strict';

/* Controllers */

var accountingDocsControllers = angular.module('accountingDocsControllers',  [ 'angular-md5'  ]);




accountingDocsApp.controller('HeaderCtrl', ['$scope', '$location', '$rootScope', 'localStorageService',
    function($scope, $location, $rootScope, localStorageService) {
        if (localStorageService.get('user_pfr')) {
            $rootScope.user = localStorageService.get('user_pfr');
            $scope.user = localStorageService.get('user_pfr');

        }

        $scope.logout = function() {
            localStorageService.clearAll();
            location.reload();
            $location.path('/login');
        }


    }]);


accountingDocsApp.controller('TypeCtrl', function ($scope, Newspaper) {


    $scope.newspapers = Newspaper.getAll();
    $scope.myData = {};
    $scope.myData.create = function(typeForm) {

        $scope.newspapers = Newspaper.add({name: typeForm.name});

    }
    $scope.myData.addNewspaper = function(newspaperForm) {

         $scope.newspapers = Newspaper.add(newspaperForm);

    }
    $scope.myData.editNewspaper = function(newspaperFormEdit) {

          $scope.newspapers = Newspaper.edit(newspaperFormEdit);

    }
    $scope.edit = function(newspaper) {

        $('#editModal').modal('show')
        $scope.newspaperFormEdit = newspaper

    }

    $scope.myData.delete = function(id) {

        $scope.newspapers = Newspaper.remove({id: id});

    }

 //   $scope.allStages = Stage.getAll({token: $rootScope.user.token});




});


accountingDocsApp.controller('TypeCurCtrl', function ($scope, $routeParams, Newspaper) {


     $scope.myData = {};

    $scope.contents = Newspaper.getContent({id: $routeParams.id});

    Newspaper.get({id: $routeParams.id}).$promise.then(function(data){
        $scope.newspaper = data;

        if (data.font == 'true') {
            $scope.class = 'bigTitle1';
        } else {
            $scope.class = 'bigTitle2';
        }

    })

    $scope.myData.addNews = function(newspaperForm) {

        var params = $.param(newspaperForm);
        $scope.contents = Newspaper.addContent({id: $routeParams.id},params)

    }

    $scope.myData.editNews = function(newspaperFormEdit) {

        var params = $.param(newspaperFormEdit);
        $scope.contents = Newspaper.editContent({id: $routeParams.id},params)

    }

    $scope.myData.delete = function(rel_id) {


    }

    $scope.edit = function(content) {

        $('#edit_modal').modal('show')
        $scope.newspaperFormEdit = content
        $scope.newspaperFormEdit.count = content.col
        $scope.newspaperFormEdit.title = content.label
        $scope.newspaperFormEdit.title_in = content.checked == 1 ? true : false
    }





});
