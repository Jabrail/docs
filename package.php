<?php

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/class.package.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

$user = new User();
if (!$user->init($_GET["token"])) return false;

if ($action == 'add_package') {

    $package = new Package();
    $package->newPackage($user->user_id);
    $response = array($package->package_json());
    echo json_encode($response);

} elseif ($action == 'show') {

    if (isset($_GET['id'])) {

        $package = new Package();
        $package->init($_GET['id']);
        $response = array($package->case_json());
        echo json_encode($response);
    }
} elseif ($action == 'delete_package') {

    if (isset($_POST['package_id'])) {

        $package = new Package();
        $package->deletePackage($_POST['package_id']);
       // echo json_encode();
    }
}  elseif ($action == 'group_case') {

    $caseIds = isset($_POST['caseIds']) ? $_POST['caseIds'] : false;

    if (count($caseIds) > 0 ) {
        $package = new Package();
        $package->newPackage($user->user_id);
        foreach($caseIds as $caseId) {
            $package->addCase($caseId);
        }
    }

} elseif ($action == 'get_all') {

    $package = new Package();

    $ids = isset($_GET['exceptions']) ? $_GET['exceptions'] : 0;
    $sort = '';

    $where = $user->user_type != 1 ? 'creator_user_id = '.$user->user_id.' AND status = 0 AND' : '';


    if (isset($_GET['sort'])) {

        $sort = $_GET['sort'] == 'sort_by_id' ? ' ORDER BY package_id DESC' : '';
    }

    $response = $package->package_get_all_json($ids, $sort, $where);

    echo json_encode($response);

} elseif ($action == 'get_all_cvp') {

    $package = new Package();

    $ids = isset($_GET['exceptions']) ? $_GET['exceptions'] : 0;
    $sort = '';

    $where = 'status = 1 AND';


    if (isset($_GET['sort'])) {

        $sort = $_GET['sort'] == 'sort_by_id' ? ' ORDER BY package_id DESC' : '';
    }

    $response = $package->package_get_all_json($ids, $sort, $where);

    echo json_encode($response);

} elseif ($action == 'send_in_cvp') {

    $package = new Package();
    $id = isset($_POST['package_id']) ? $_POST['package_id'] : 0;
    $package->init($id);
    $sort = '';

    $response = $package->send_in_cvp($user->user_id);

    echo json_encode($response);
}

function getFIO($conn, $npers){

    $queryStr = "SELECT FA, IM, OT FROM PF.MAN WHERE PF.MAN.NPERS = '$npers'";
    $resQuery = FALSE;
    $_stmt = db2_prepare($conn, $queryStr);
    if ($_stmt) {
        if (db2_execute($_stmt)) {
            while ($row = db2_fetch_assoc($_stmt)) {
                $resQuery['last_name'] = $row['FA'];
                $resQuery['name'] = $row['IM'];
                $resQuery['patronymic'] = $row['OT'];
            }
        }
        db2_free_stmt($_stmt);
    }
    return $resQuery;
}
?>
