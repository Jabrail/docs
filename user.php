<?php

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

/*if ($action != 'login') {

    $user = new User();
    if (!$user->init($_GET["token"])) {
        echo json_encode('bad token');
        return false;
    }
}*/

if ($action == 'login') {

    $user = new User();
    $user->login($_POST['login'] , $_POST['pass']);
    $response = $user->user_json();
    echo json_encode($response);


} elseif ($action == 'get_all') {

        $user = new User();
    $tokens = isset($_GET['exceptions']) ? $_GET['exceptions'] : "'0'";
    $response = $user->user_get_all_json();
        echo json_encode($response);

} elseif ($action == 'add_user') {

       $user = new User();
       $user->newUser();
       $user->setParams();
       $user->update();
} elseif ($action == 'delete_user') {

       $user = new User();
       $token = $_POST['user_token'];
       $user->deleteUser($token);
} elseif ($action == 'addStage') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $usr = new User();
    $response = $usr->addStage($data->user_id,$data->stage_id);
    echo json_encode($response);

} elseif ($action == 'getStages') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $id = isset($data->id) ? $data->id : 0;
    $usr = new User();
    $response = $usr->getStages($id);
    echo json_encode($response);

}elseif ($action == 'removeStage') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $id = isset($data->id) ? $data->id : 0;
    $user_id = isset($data->user_id) ? $data->user_id : 0;
    $usr = new User();
    $response = $usr->removeStage($id, $data->user_id);
    echo json_encode($response);

}

function check() {

}
?>
