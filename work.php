<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 5:11
 */

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/class.work.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

$user = new User();
// if (!$user->init($_GET["token"])) return false;

if ($action == 'create') {

    $work = new Work();
    $case_id = $_POST['case_id'];
    $work->newWorkRel($case_id, 1);

   return true;

} elseif ($action == 'take') {

    $work = new Work();
    $case_id = $_POST['case_id'];
    $work->takeInWork($case_id, 1);

    return true;

} elseif ($action == 'next') {

    $work = new Work();
    $case_id = $_POST['case_id'];
    $work->sendNextStage($case_id, 1);

    return true;

}
?>
