'use strict';

/* Services */

var accountingDocsServices = angular.module('accountingDocsServices', ['ngResource']);

accountingDocsServices.factory('Phone', ['$resource',
    function($resource){
        return $resource('phones/:phoneId.json', {}, {
            query: {
                method:'GET',
                params:{phoneId:'phones'},
                isArray:true}
        });
    }]);

/*
accountingDocsServices.factory('CaseResource', ['$resource',
    function($resource , token){
        return $resource('http://localhost/apps_for_pfr/case.php', {}, {
            query: {
                method:'GET',
                isArray:true
            },
            queryAdd: {
                url: 'http://localhost/apps_for_pfr/case.php?a=add_cases',
                method:'POST',
                isArray:true
            }
        });
    }]);
*/
