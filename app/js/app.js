'use strict';

/* App Module */

var accountingDocsApp = angular.module('accountingDocsApp', [
  'ngRoute',
  'accountingDocsAnimations',

  'accountingDocsControllers',
  'accountingDocsFilters',
  'accountingDocsServices',
  'LocalStorageModule'
]);

accountingDocsApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'partials/user.html',
        controller: 'UserCtrl'
      }).
      when('/case', {
        templateUrl: 'partials/cases.html',
        controller: 'CasesCtrl'
      }).
      when('/CVP', {
        templateUrl: 'partials/CVP.html',
        controller: 'CVPCtrl'
      }).
      when('/ONIPP', {
        templateUrl: 'partials/ONIPP.html',
        controller: 'ONIPPCtrl'
      }).
      when('/users', {
        templateUrl: 'partials/users.html',
        controller: 'UsersCtrl'
      }).
      when('/create', {
        templateUrl: 'partials/createCase.html',
        controller: 'CreateCaseCtrl'
      }).
      otherwise({
        redirectTo: '/login'
      });

  }]);
