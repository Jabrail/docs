'use strict';

/* Controllers */

var accountingDocsControllers = angular.module('accountingDocsControllers',  [ 'angular-md5'  ]);

accountingDocsApp.controller('NavigationCtrl', ['$scope', '$location', '$rootScope', 'localStorageService',
    function($scope, $location, $rootScope, localStorageService) {


        if (localStorageService.get('user_pfr')) {
            $rootScope.user = localStorageService.get('user_pfr');

            if ($rootScope.user.user_type == 1) {
                $location.path('/case');
            }
            else if($rootScope.user.user_type == 2) {
                $location.path('/CVP');
            }
            else if($rootScope.user.user_type == 3) {
                $location.path('/ONIPP');
            }

        } else {
            $location.path('/login');
        }

    }]);

accountingDocsApp.controller('CasesCtrl', function ($scope, $rootScope, $http , $interval, $location, localStorageService) {

    if (localStorageService.get('user_pfr')) {
        $rootScope.user = localStorageService.get('user_pfr');

        if($rootScope.user.user_type == 2) {
            $location.path('/CVP');
        }
        else if($rootScope.user.user_type == 3) {
            $location.path('/ONIPP');
        }
    } else {
        $location.path('/login');
    }

/*
    CaseResource.query({a: 'get_all', token: $rootScope.user.user_token, first: '1'},).$promise.then(function(data) {
       $scope.cases = data;
    });
*/

    $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token+'&first=1').success(function(data) {
        $scope.cases = data;
    });
    $scope.orderProp = 'case_create_date';
    var timer;
    $scope.myData = {};
    $scope.myData.doClick = function() {

        var type_id = $('#type_id').val();
        var case_last_name = $('#case_last_name').val();
        var case_name = $('#case_name').val();
        var case_patronymic = $('#case_patronymic').val();
        var url = 'http://localhost/apps_for_pfr/case.php?a=add_cases&token='+$rootScope.user.user_token;
        var params =  $.param({'type_id': type_id, 'case_last_name': case_last_name, 'case_name':case_name, 'case_patronymic': case_patronymic});

/*
        CaseResource.query(params).$promise.then(function(data) {
            $scope.cases = data;
        });
*/

        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {

            CaseResource.query({a: 'get_all', token: $rootScope.user.user_token, first: '1'}).$promise.then(function(newData) {
                newData.forEach(function(item) {
                    $scope.cases.push(item);
                })
            });
        });
    }

    timer = $interval(function() {
        $http({method: 'get',
            url: 'http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token ,
            cache: false
        }).success(function(data) {
            data.forEach(function(item) {
                $scope.cases.push(item);
            })
        });
    }, 3000);

});

accountingDocsApp.controller('CreateCaseCtrl', function ($scope, $rootScope, $http , $interval, $location, localStorageService) {

    if (localStorageService.get('user_pfr')) {
        $rootScope.user = localStorageService.get('user_pfr');

        if($rootScope.user.user_type == 2) {
            $location.path('/CVP');
        }
        else if($rootScope.user.user_type == 3) {
            $location.path('/ONIPP');
        }
    } else {
        $location.path('/login');
    }

    $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token+'&first=1').success(function(data) {
        $scope.cases = data;
    });

    $scope.orderProp = 'case_create_date';
    var timer;
    $scope.myData = {};
    $scope.myData.doClick = function() {

        var type_id = $('#type_id').val();
        var case_last_name = $('#case_last_name').val();
        var case_name = $('#case_name').val();
        var case_patronymic = $('#case_patronymic').val();
        var url = 'http://localhost/apps_for_pfr/case.php?a=add_cases&token='+$rootScope.user.user_token;
        var params =  $.param({'type_id': type_id, 'case_last_name': case_last_name, 'case_name':case_name, 'case_patronymic': case_patronymic});
        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {

            $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token).success(function(newData) {
                newData.forEach(function(item) {
                    $scope.cases.push(item);
                })
            });
        });
    }

    timer = $interval(function() {
        $http({method: 'get',
            url: 'http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token ,
            cache: false
        }).success(function(data) {
            data.forEach(function(item) {
                $scope.cases.push(item);
            })
        });
    }, 3000);

});

accountingDocsApp.controller('UserCtrl', ['$scope', '$http', 'md5', '$location', '$rootScope', 'localStorageService',
    function($scope, $http  , md5, $location, $rootScope, localStorageService) {

        if (localStorageService.get('user_pfr')) {
            $rootScope.user = localStorageService.get('user_pfr');
            $location.path('/case');
        }
        else {

            $scope.submit = function(user) {

                var params =  $.param({login: user.login , pass: md5.createHash(user.pass)});
                $http({
                    url: 'http://localhost/apps_for_pfr/user.php?a=login',
                    method: "post",
                    data: params,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data) {

                    if (data.user_token != '') {
                        $rootScope.user = data;
                        localStorageService.set('user_pfr' , data);
                        $location.path('/NavigationCtrl');
                    }
                    else {
                        $location.path('/login');
                    }
                });
            }
        }

        $scope.logout = function() {
            $rootScope.user = null;
            localStorageService.clearAll();
        }

    }]);

accountingDocsApp.controller('HeaderCtrl', ['$scope', '$location', '$rootScope', 'localStorageService',
    function($scope, $location, $rootScope, localStorageService) {

        if (localStorageService.get('user_pfr')) {
            $rootScope.user = localStorageService.get('user_pfr');
        }

        $scope.logout = function() {
            localStorageService.clearAll();
            $location.path('/login');
        }

    }]);

accountingDocsApp.controller('ONIPPCtrl', function ($scope, $rootScope, $http , $interval, $location, localStorageService) {

    if (localStorageService.get('user_pfr')) {
        $rootScope.user = localStorageService.get('user_pfr');

        if($rootScope.user.user_type == 2) {
            $location.path('/CVP');
        }
        else if($rootScope.user.user_type == 3) {
            $location.path('/ONIPP');
        }

    } else {
        $location.path('/login');
    }

    $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token+'&first=1').success(function(data) {
        $scope.cases = data;
    });

    var timer;
    $scope.myData = {};
    $scope.myData.doClick = function() {

        var type_id = $('#type_id').val();
        var case_last_name = $('#case_last_name').val();
        var case_name = $('#case_name').val();
        var case_patronymic = $('#case_patronymic').val();
        var url = 'http://localhost/apps_for_pfr/case.php?a=add_cases&token='+$rootScope.user.user_token;
        var params =  $.param({'type_id': type_id, 'case_last_name': case_last_name, 'case_name':case_name, 'case_patronymic': case_patronymic});
        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {

            $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token).success(function(newData) {
                newData.forEach(function(item) {
                    $scope.cases.push(item);
                })
            });
        });
    }

    timer = $interval(function() {
        $http({method: 'get',
            url: 'http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token ,
            cache: false
        }).success(function(data) {
            data.forEach(function(item) {
                $scope.cases.push(item);
            })
        });
    }, 3000);
});

accountingDocsApp.controller('CVPCtrl', function ($scope, $rootScope, $http , $interval, $location, localStorageService) {

    if (localStorageService.get('user_pfr')) {
        $rootScope.user = localStorageService.get('user_pfr');

        if($rootScope.user.user_type == 2) {
            $location.path('/CVP');
        }
        else if($rootScope.user.user_type == 3) {
            $location.path('/ONIPP');
        }

    } else {
        $location.path('/login');
    }

    $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token+'&first=1').success(function(data) {
        $scope.cases = data;
    });


    var timer;
    $scope.myData = {};
    $scope.myData.doClick = function() {

        var type_id = $('#type_id').val();
        var case_last_name = $('#case_last_name').val();
        var case_name = $('#case_name').val();
        var case_patronymic = $('#case_patronymic').val();
        var url = 'http://localhost/apps_for_pfr/case.php?a=add_cases&token='+$rootScope.user.user_token;
        var params =  $.param({'type_id': type_id, 'case_last_name': case_last_name, 'case_name':case_name, 'case_patronymic': case_patronymic});
        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {

            $http.get('http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token).success(function(newData) {
                newData.forEach(function(item) {
                    $scope.cases.push(item);
                })
            });
        });
    }

    timer = $interval(function() {
        $http({method: 'get',
            url: 'http://localhost/apps_for_pfr/case.php?a=get_all&token='+$rootScope.user.user_token ,
            cache: false
        }).success(function(data) {
            data.forEach(function(item) {
                $scope.cases.push(item);
            })
        });
    }, 3000);

});

accountingDocsApp.controller('UsersCtrl', function ($scope, $rootScope, $http , $location, localStorageService) {

    if (localStorageService.get('user_pfr')) {
        $rootScope.user = localStorageService.get('user_pfr');

        if($rootScope.user.user_type != 1) {
            $location.path('/login');
        }
    } else {
        $location.path('/login');
    }

    $http.get('http://localhost/apps_for_pfr/user.php?a=get_all&token='+$rootScope.user.user_token+'&first=1').success(function(data) {
        $scope.users = data;
    });

    $scope.myData = {};
    $scope.myData.doClick = function(userForm) {


        userForm.user_pass = generatePassword();

        var url = 'http://localhost/apps_for_pfr/user.php?a=add_user&token='+$rootScope.user.user_token;
        var params =  $.param(userForm);
        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {

            $http.get('http://localhost/apps_for_pfr/user.php?a=get_all&token='+$rootScope.user.user_token).success(function(newData) {

                $scope.users = newData;
            });
        });

    }

    $scope.myData.deleteUser = function(token) {

        var url = 'http://localhost/apps_for_pfr/user.php?a=delete_user&token='+$rootScope.user.user_token;
        var params =  $.param({'user_token': token});
        $http({
            url: url,
            method: "post",
            data: params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {
            $http.get('http://localhost/apps_for_pfr/user.php?a=get_all&token='+$rootScope.user.user_token).success(function(newData) {
                $scope.users = newData;
            });
        });


    }

    function generatePassword() {
        var length = 6,
            charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }


});

accountingDocsApp.controller('LeftMenuRowCtrl', function ($scope, $rootScope, $http , $location) {


    $scope.redirect = function(rout) {

        $location.path(rout);

    }

    $scope.checkUserType = function() {

        if ($rootScope.user.user_type == 1) {
            return true;
        }
        else {
            return false;
        }
    }



});

