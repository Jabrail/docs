<?php

	session_start();
	
	## development
	error_reporting(-1);
	if(!ini_get('display_errors')) ini_set('display_errors', true);
	if(!ini_get('display_startup_errors')) ini_set('display_startup_errors', true);
	if(!ini_get('log_errors')) ini_set('log_errors', true);
	ini_set('date.timezone', "Europe/Moscow");
    ini_set('upload_tmp_dir', "/tmp");
	
	## production
	/*error_reporting(E_ALL);
	if(!ini_get('display_errors')) ini_set('display_errors', false);
	if(!ini_get('display_startup_errors')) ini_set('display_startup_errors', false);
	if(!ini_get('log_errors')) ini_set('log_errors', true);*/
	
	require_once($rootPath."config.php");
	require_once($rootPath."constants.php");
	require_once($rootPath."functions.php");
	require_once($rootPath.INC_DB.$dbType.".php");

	// Make the database connection.
	$db = new $sql_db();
	$result = $db->sql_connect($dbHost, $dbUser, $dbPass, $dbName);
	
	if(!$db->db_connect_id)
	{
		die("Нет доступа к базе данных - ".$result['message'].' ('.$result['code'].')');
	}
	else
	{
		$db->sql_query("SET NAMES utf8");
	}
	
	require_once($rootPath.INC_DIR."class.settings.php");
	
?>