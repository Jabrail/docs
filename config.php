<?php
	
	// Mysql - remote
	$dbType = "mysqli";
	$dbHost = "localhost";
	$dbName = "verst";
	$dbUser = "root";
	$dbPass = "123456";

	$config["folder"] = "/";
	
	$config["admin_group"][1] = "Администратор";
	$config["admin_group"][2] = "Админ города";
	$config["admin_group"][3] = "Компания";

	$config["order_status"][0] = "Поиск Такси";
	$config["order_status"][1] = "Такси найдено";
	$config["order_status"][2] = "Такси приехало";
	$config["order_status"][3] = "Взял клиента";
	$config["order_status"][4] = "Выполнен";
	$config["order_status"][5] = "Отменен Таксистом";
	$config["order_status"][6] = "Отменен Клиентом";
	
	$config["class_auto"][1] = "Стандарт";
	$config["class_auto"][2] = "Комфорт";
	$config["class_auto"][3] = "Бизнес";

	$config["timeout"] = 300;

	$config["log_status"][1] = "<span class='text-success'>Инфо</span>";
	$config["log_status"][2] = "<span class='text-danger'>Ошибка</span>";
	
	$config["site_name"] = "Autoway";
	
?>
