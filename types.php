<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 5:11
 */

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/newspaper.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

$user = new User();
// if (!$user->init($_GET["token"])) return false;

if ($action == 'add') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    $type = new Types();
    $type->newType();
    $type->type_name = isset($data->type_name) ? $data->type_name : '';
    $type->update();

    $response = $type->getAll();
    echo json_encode($response);

} elseif ($action == 'addStage') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $type = new Types();
    $response = $type->addStage($data->type_id,$data->num,$data->stage_id);
    echo json_encode($response);

} elseif ($action == 'getStages') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $id = isset($data->id) ? $data->id : 0;
    $type = new Types();
    $response = $type->getStages($id);
    echo json_encode($response);

}elseif ($action == 'removeStage') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $id = isset($data->id) ? $data->id : 0;
    $type_id = isset($data->type_id) ? $data->type_id : 0;
    $type = new Types();
    $response = $type->removeStage($id, $data->type_id);
    echo json_encode($response);

} elseif ($action == 'show') {


} elseif ($action == 'getAll') {

    $type = new Types();
    $response = $type->getAll();
    echo json_encode($response);

}elseif ($action == 'remove') {

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    $type = new Types();
    $type->init($data->id);
    $response = $type->remove();

    $response = $type->getAll();
    echo json_encode($response);
}
?>
