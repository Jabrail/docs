<?php
/**
 * Created by PhpStorm.
 * User: dzabrail
 * Date: 10.11.14
 * Time: 5:11
 */

$rootPath = "./";
require_once($rootPath."common_responder.php");
require_once($rootPath."/includes/class.stage.php");
require_once($rootPath."/includes/class.user.php");

$action = isset($_GET["a"]) && !empty($_GET["a"]) ? $_GET["a"] : false;

$token = isset($_GET["token"]) ? $_GET["token"] : '';
$token = isset($_POST['token']) ? $_POST['token'] : $token;
//echo $token;
$user = new User();

 if (!$user->init($token)) return false;

if ($action == 'add') {

    $stage = new Stage();
    $stage->newStage();
    $stage->stage_name = isset($_POST['stage_name']) ? $_POST['stage_name'] : '';
    $stage->update();

    $response = $stage->getAll();
    echo json_encode($response);

} elseif ($action == 'getStage') {

    $stage = new Stage();
    $stage->init($_GET['stage_id']);


    $response = $stage->stage_json();
    echo json_encode($response);

} elseif ($action == 'getAll') {

    $stage = new Stage();
    $response = $stage->getAll();
    echo json_encode($response);
} elseif ($action == 'allForUser') {

    $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : $user->user_id;
    $stage = new Stage();
    $response = $stage->allForUser($user_id);
    echo json_encode($response);

} elseif ($action == 'addForUser') {

    $stage_id = $_POST['stage_id'];
    $user_id = $_POST['user_id'];
    $stage = new Stage();
    $response = $stage->addForUser($user_id, $stage_id);
    echo json_encode($true);

} elseif ($action == 'removeForUser') {

    $id = $_POST['id'];
    $stage = new Stage();
    $response = $stage->removeForUser($id);
  //  echo json_encode($response);

} elseif ($action == 'remove') {

    $stage = new Stage();
    $stage->init($_POST['id']);
    $response = $stage->remove();

    $response = $stage->getAll();
    echo json_encode($response);}
?>
